package dico;



import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import javax.swing.JOptionPane;

import rdc.RdC;

public class VerificateurOrthographe {

	Vector<String> dico;
	public VerificateurOrthographe(String dictionnaire) {
		this.dico = lireDictionnaire(dictionnaire);
	}

	public boolean estBienOrthographie(String mot){
		int i,j;
		i = 0;
		j = dico.size()-1;
		int m;
		int test;
		while (i<=j){
			m = (i+j)/2;
			test = comparaisonMot(mot,dico.elementAt(m));
			if (test == -1)
				j = m-1;
			else if (test == 1)
				i = m+1;
			else return true;				
		}		
		return false;
	}

	public int comparaisonMot(String mot1, String mot2) {
		int i;
		i = 0;
		int min;
		if (mot1.length() < mot2.length())
			min = mot1.length();
		else min = mot2.length();

		while ((i < min) && (mot1.charAt(i) == mot2.charAt(i))){
			i++;
		}
		if (i == min){
			if (mot1.length() < mot2.length())
				return -1;
			else if (mot1.length() > mot2.length())
				return 1;
			return 0;
		}
		if (mot1.charAt(i) < mot2.charAt(i))
			return -1;
		else if (mot1.charAt(i) > mot2.charAt(i))
			return 1;
		return 0;
	}

	public Vector<String> lireDictionnaire(String dictionnaire){
		Vector<String> retour = new Vector<String>();
		File f = new File(dictionnaire);
		String s;
		try {
			FileReader fr = new FileReader(f);
			BufferedReader br= new BufferedReader(fr);
			while((s = br.readLine()) != null){
				retour.add(s);
			}
			return retour;
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(null,RdC.elementsTraduction[93]+dictionnaire+RdC.elementsTraduction[94],RdC.elementsTraduction[95],JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,RdC.elementsTraduction[93]+dictionnaire+RdC.elementsTraduction[96],RdC.elementsTraduction[97],JOptionPane.ERROR_MESSAGE);
		}
		return null;
	}


        public String random_choose(int longueur){
            int random=(int)(Math.random()*(dico.size()-1));
            boolean monte=true;
            String sol="";
            if (random<dico.size()/2)
                monte=false;

            boolean ok=false;
            while(!ok){
                if(dico.get(random).length()==longueur){
                    ok=true;
                    sol=dico.get(random);
                }
                else
                    if(monte)
                        random--;
                    else
                        random++;
            }
            return sol;
        }



	//TO SUPPRESS
	/*public static void main(String[] args){
		VerificateurOrthographe v;

		v = new VerificateurOrthographe("dico_francais.txt");
		/*String mot = "ordinateur";
		System.out.println(v.estBienOrthographie("ORDINATEUR"));
	}*/
}


