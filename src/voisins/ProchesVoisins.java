package voisins;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import rdc.RdC;


import baseApp.*;

public class ProchesVoisins extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Vector<Short> nouvelElement;
	BaseApprentissage base;
	distanceEdition.DistanceEdition distance_ed;
	public int k;

	Voisin_options option ;
	Statistiques_voisin sv;

	JPanel principal = new JPanel();
	JTabbedPane tableOnglet = new JTabbedPane();
	JTextArea text;
	ImageLettre im;
	JPanel chercher;
	JButton init,good,bad;
	JPanel elem_trouver;
	JPanel optio,head;
	JLabel titre;
	JLabel test_fiab=new JLabel(RdC.elementsTraduction[41]);

	Vector<tab_stat> vp=new Vector<tab_stat>();

	private JScrollPane scroll;

	public ProchesVoisins(Vector<Short> nouvelElement, BaseApprentissage base, distanceEdition.DistanceEdition distance_ed)
	{
		this.sv=new Statistiques_voisin();
		this.option=new Voisin_options(distance_ed, base, this);
		this.setLayout(new GridLayout(1, 1));
		this.principal.setLayout(new GridLayout(2, 1));
		this.chercher = new JPanel();
		this.nouvelElement = nouvelElement;
		this.distance_ed = distance_ed;
		this.base = base;
		this.k = 0;
		this.elem_trouver=new JPanel();
		this.im = new ImageLettre();
		this.optio=new JPanel();
		this.optio.setLayout(new BorderLayout());
		this.optio.setPreferredSize(new Dimension(200,200));
		this.head=new JPanel();
		this.good=new JButton(RdC.elementsTraduction[42]);
		this.bad=new JButton(RdC.elementsTraduction[43]);
		this.good.setEnabled(false);
		good.setToolTipText(RdC.elementsTraduction[44]);
		this.bad.setEnabled(false);
		bad.setToolTipText(RdC.elementsTraduction[45]);
		//this.titre=new JTextArea("Resultat de la recherche de caractere \n " + "par la methode du plus proche voisin");
		//this.titre.setEditable(false);
		this.titre=new JLabel(RdC.elementsTraduction[46]);
		this.titre.setFont(new Font("TimesRoman", Font.PLAIN,12));
		this.text=new JTextArea(1, 1);
		this.text.setEditable(false);
		//this.text = new JLabel(new javax.swing.ImageIcon(im.getTab_img().get('0')));
		this.text.setPreferredSize(new Dimension(220, 200));
		this.scroll = new JScrollPane(this.elem_trouver);
		this.init = new JButton(RdC.elementsTraduction[18]);
		init.setToolTipText(RdC.elementsTraduction[47]);
		this.chercher.setLayout(new BorderLayout());
		this.head.add(this.titre);
		this.chercher.add(this.head,BorderLayout.NORTH);
		this.optio.add(this.init, BorderLayout.NORTH);
		JPanel bas=new JPanel();
		bas.add(this.test_fiab);
		JPanel bas1=new JPanel();
		bas1.add(this.good);
		bas1.add(this.bad);
		JPanel bas2=new JPanel();
		bas2.add(bas);
		bas2.add(bas1);
		bas2.setPreferredSize(new Dimension(70,70));
		this.optio.add(bas2,BorderLayout.SOUTH);
		this.chercher.add(this.text,BorderLayout.WEST);
		this.chercher.add(this.optio,BorderLayout.EAST);
		this.principal.add(this.chercher);
		this.principal.add(this.scroll);

		this.add_listener();

		this.tableOnglet = new JTabbedPane();
		this.tableOnglet.add(RdC.elementsTraduction[48], this.principal);
		this.tableOnglet.add(RdC.elementsTraduction[49], this.option);
		this.tableOnglet.add(RdC.elementsTraduction[50],this.sv);

		this.add(tableOnglet);
	}

	public void add_listener()
	{
		this.init.addActionListener(this);
		this.good.addActionListener(this);
		this.bad.addActionListener(this);
	}

	public Vector<Element> plusProchesVoisins2()
	{
		//System.out.println("K="+this.k);
		Vector<Element> tab_caractere = new Vector<Element>();
		TreeSet<Element> tab = new TreeSet<Element>(new ComparateurElements(distance_ed, nouvelElement));
		int i;
		i = 0;
		while ((i < this.base.getBase().size()) && (i < this.k))
		{// ajout de k elements : pas necessairement les plus petits
			tab.add(this.base.getBase().elementAt(i));
			i++;
		}
		int j, d1, d2;
		if (base.getBase().size() >= this.k){
			boolean[] tags = new boolean[base.getBase().size() - this.k];// si tags[i] est faux, il n'est pas necessaire de calculer la distance du nouvelElement a l'element i+k de la base
			for (i = 0; i < tags.length; i++)
			{
				tags[i] = true;
			}
			int distanceAMax, distanceAI;
			Element max;
			i = this.k;
			j = 0;
			while (i < this.base.getBase().size())
			{
				// ajout du reste que si necessaire
				if (tags[j])
				{
					max = tab.last();
					distanceAI = distance_ed.distance(nouvelElement, base.getBase().elementAt(i).getContour());
					distanceAMax = distance_ed.distance(nouvelElement, max.getContour());
					if (distanceAI > distanceAMax)
					{
						// si ca ne fonctionne pas on
						// peut accelerer le calcul pour
						// les cas suivants en
						// supprimant les cas
						// impossibles
						// System.out.println("acceleration");
						// comme l'element a l'indice i ne convient pas :
						// on peut dire que tous les points suffisements proches de
						// l'element a l'indice i
						// n'appartiendront pas aux k plus proches voisins de
						// l'element nouveau
						// de meme tous les elements suffisement eloignes de de
						// l'element a l'indice i
						// n'appartiendront pas aux k plus proches voisins de
						// l'element nouveau

						// suffisament proche de i c'est en faite : distance de i
						// aux points < distance(nouveau,i) - distance(nouveau,max)
						// suffisament eloigne de i c'est en fait : distance de i
						// aux points > distance(nouveau,i) + distance(nouveau,max)
						d1 = distanceAI - distanceAMax;
						d2 = distanceAI + distanceAMax;
						for (int l = i; l < this.base.getBase().size(); l++)
						{
							if ((base.distancesDejaCalculees.elementAt(i).elementAt(l) < d1)
									|| (base.distancesDejaCalculees.elementAt(i).elementAt(l) > d2))
							{
								tags[l - this.k] = false;
							}
						}

					}
					else
					{
						tab.add(base.getBase().elementAt(i));
					}
				}
				i++;
				j++;

			}
		}
		Iterator<Element> it = tab.iterator();
		j = 0;
		while ((it.hasNext()) && (j < this.k))
		{
			tab_caractere.add((it.next()));
			// System.err.println("j="+j);
			j++;
		}

		return tab_caractere;
	}

	public Vector<Element> PlusProchesVoisins()
	{
		Vector<Element> tab_caractere = new Vector<Element>();

		TreeSet<Element> tab = new TreeSet<Element>(new ComparateurElements(distance_ed, nouvelElement));

		for (int i = 0; i < this.base.getBase().size(); i++)
		{
			tab.add(this.base.getBase().elementAt(i));
		}

		Iterator<Element> i = tab.iterator();
		int j = 0;
		while ((i.hasNext()) && (j < this.k))
		{
			tab_caractere.add(i.next());
			//System.out.println("Caractere"+i.next()); //Affichage du short
			j++;
		}
		return tab_caractere;
	}

	public Vector<JLabel> affiche_tableau(Vector<Character> vc)
	{
		Vector<JLabel> vjl = new Vector<JLabel>();
		for (int i = 0; i < vc.size(); i++)
		{
			vjl.add(new JLabel((im.getTab_img().get(vc.elementAt(i)))));
		}
		return vjl;
	}

	public void changeK(int k){
		this.option.setK(k);
	}
	
	
	public void settext(String lettre)
	{
		this.text.setText(lettre);
	}

	public void affiche_voisin(Vector<Character> va)
	{
		for (int i = 0; i < va.size(); i++)
		{
			System.out.println("" + va.elementAt(i));
		}
	}

	public Character determine_lettre(Vector<Element> vc)
	{
		this.good.setEnabled(true);
		this.bad.setEnabled(true);
		table_compte_caractere tcc = new table_compte_caractere();
		this.vp=new Vector<tab_stat>();
		for (int i = 0; i < vc.size(); i++) {
			tcc.ajoute_caractere(vc.elementAt(i).getEtiquette());
		}
		//System.out.println("La taille "+vc.size());
		for(int i=0;i<tcc.getTab_car().size();i++)
		{
			double percent=(double)((double)tcc.getTab_car().elementAt(i).getCompte()/(double)vc.size())*100.0;
			this.vp.add(new tab_stat(percent,tcc.getTab_car().elementAt(i).getCaractere() ));
		}
		this.sv.revalidate();
		//System.out.println("Lettre trouver:"+tcc.getTab_car().elementAt(0));
		compte_caractere cmax = tcc.getTab_car().elementAt(0);
		for (int i = 1; i < tcc.getTab_car().size(); i++)
		{
			if (tcc.getTab_car().elementAt(i).getCompte() > cmax.getCompte())
			{
				cmax = tcc.getTab_car().elementAt(i);
			}
		}
		this.sv.setTab(this.vp);
		this.sv.creer_diag(vc.size());
		this.sv.creer_diag(vc.size());

		return cmax.getCaractere();
	}

	public void effacer()
	{
		this.good.setEnabled(false);
		this.bad.setEnabled(false);
		//ImageIcon ic = new javax.swing.ImageIcon(im.getTab_img().get('0'));
		//this.text.setIcon(ic);
		this.text.setText("");
		this.chercher.add(this.text);
		this.revalidate();
		this.scroll.removeAll();
		this.revalidate();
		this.repaint();
	}

	public void actionPerformed(ActionEvent a)
	{
		if (a.getSource() == init)
		{
			effacer();
		}
		if(a.getSource()==this.good)
		{
			this.good.setEnabled(false);
			this.bad.setEnabled(false);
			this.sv.setReussit();
			this.sv.setEssais();
		}
		if(a.getSource()==this.bad)
		{
			this.good.setEnabled(false);
			this.bad.setEnabled(false);
			this.sv.setErreur();
			this.sv.setEssais();
		}
		this.repaint();
	}

	public Vector<Element> chercher_voisin()
	{
		Vector<Element> vc =new Vector<Element>();

		if (this.nouvelElement == null)
		{
			ImageIcon img = new ImageIcon("./images/stop.png");
			JOptionPane.showMessageDialog(null,RdC.elementsTraduction[51], RdC.elementsTraduction[52],JOptionPane.ERROR_MESSAGE, img);
		} else {

			this.k = this.option.getK();
			if (option.getV1())
			{
				vc = this.plusProchesVoisins2();
			}
			else
			{
				vc = this.PlusProchesVoisins();	
			}

			//ImageIcon ic = new javax.swing.ImageIcon(im.getTab_img().get(this.determine_lettre(vc)));
			//this.text.setIcon(ic);
			this.text.setText("  "+Character.toString(this.determine_lettre(vc)));
			this.text.setFont(new Font("TimesRoman", Font.PLAIN, 140));
			this.text.setEditable(false);

			this.chercher.add(this.text,BorderLayout.WEST);
			this.revalidate();

			JTabbedPane jt=((JTabbedPane)this.getComponent(0));
			JPanel jp=(JPanel)jt.getComponent(0);
			jp.remove(1);

			this.elem_trouver=new JPanel();

			for (int i = 0; i < vc.size(); i++)
			{
				this.elem_trouver.add(new JLabel(new ImageIcon(vc.elementAt(i).getChemin())));
				this.revalidate();
			}
			this.scroll=new JScrollPane(this.elem_trouver);
			jp.add(this.scroll);
			this.revalidate();
		}
		this.repaint();
		return vc;
	}

	public Vector<Short> getNouvelElement()
	{
		return nouvelElement;
	}

	public void setNouvelElement(Vector<Short> nouvelElement)
	{
		this.nouvelElement = nouvelElement;
	}

	public void setBase(BaseApprentissage ba)
	{
		this.base=ba;
	}

	public void setK(int k)
	{
		this.k = k;
	}
}
