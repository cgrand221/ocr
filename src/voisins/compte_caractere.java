package voisins;

import rdc.RdC;

public class compte_caractere {
	
	int compte;
	char caractere;
	
	public compte_caractere(int compte, char caractere) {
		this.compte = compte;
		this.caractere = caractere;
	}
	public int getCompte() {
		return compte;
	}
	public void setCompte(int compte) {
		this.compte = compte;
	}
	public char getCaractere() {
		return caractere;
	}
	public void setCaractere(char caractere) {
		this.caractere = caractere;
	}
	public String toString(){
		String s=RdC.elementsTraduction[39]+this.caractere+RdC.elementsTraduction[40]+this.compte;
		return s;
	}
}
