package voisins;

import java.util.Vector;

public class table_compte_caractere {

	Vector<compte_caractere> tab_car;
	
	public table_compte_caractere() {
		tab_car =new Vector<compte_caractere>();
	}

	public Vector<compte_caractere> getTab_car() {
		return tab_car;
	}

	public void setTab_car(Vector<compte_caractere> tabCar) {
		tab_car = tabCar;
	}
	
	public void ajoute_caractere(char caractere){
		int i;
		int ajoute=0;
		for(i=0;i<this.tab_car.size();i++){
			if(this.tab_car.elementAt(i).getCaractere()==caractere){
				ajoute=1;
				this.tab_car.elementAt(i).setCompte(this.tab_car.elementAt(i).getCompte()+1);
			}
		}
		if(ajoute==0){
			
			this.tab_car.add(new compte_caractere(1, caractere));
		}
	}
	
	public void affiche_tableau_caractere(){
		for(int i=0;i<this.tab_car.size();i++){
			System.out.println(this.tab_car.elementAt(i).toString());
		}
	}
	
}
