package voisins;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.Random;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.Plot;
import org.jfree.data.general.DefaultPieDataset;

import rdc.RdC;

import java.awt.Font;

public class Statistiques_voisin extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	int nb_essais;
	int nb_reussites;
	int nb_erreurs;

	JPanel local;
	JPanel general;
	JPanel statloc;
	
	JLabel ogen = new JLabel(RdC.elementsTraduction[53]);
	JLabel oloc = new JLabel(RdC.elementsTraduction[54]);
	
	JLabel lab_essais;
	JLabel lab_reuss;
	JLabel lab_err,fiabilite;
	JButton init=new JButton(RdC.elementsTraduction[55]);
	
	Vector<tab_stat> vp;

	public Statistiques_voisin() {
		this.lecture("statsgen");
		this.lab_essais=new JLabel(RdC.elementsTraduction[56]+this.nb_essais);
		this.lab_reuss=new JLabel(RdC.elementsTraduction[57]+this.nb_reussites);
		this.lab_err=new JLabel(RdC.elementsTraduction[58]+this.nb_erreurs);
		if(this.getFiabilite().equals(RdC.elementsTraduction[59])){
			this.fiabilite=new JLabel(RdC.elementsTraduction[60]+this.getFiabilite());
		}else{
			this.fiabilite=new JLabel(RdC.elementsTraduction[60]+this.getFiabilite()+"%");
		}
		this.setLayout(new BorderLayout());
		this.vp = new Vector<tab_stat>();
		this.local = new JPanel();
		this.statloc = new JPanel();
		this.local.setLayout(new BorderLayout());
		this.local.add(this.oloc, BorderLayout.NORTH);
		this.local.add(new JPanel(), BorderLayout.WEST);
		this.creer_diag(0);
		this.local.add(this.statloc);
		this.general = new JPanel();
		this.general.setLayout(new GridLayout(4,2));
		this.general.setPreferredSize(new Dimension(100,100));
		this.general.add(this.ogen);
		this.general.add(new JPanel());	
		
		this.general.add(this.lab_essais);
		this.general.add(this.fiabilite);	
		this.general.add(this.lab_reuss);
		this.general.add(this.init);
		this.general.add(this.lab_err);

		this.ogen.setFont(new Font("TimesRoman", Font.PLAIN,16));
		this.oloc.setFont(new Font("TimesRoman", Font.PLAIN,16));
		
		this.add(this.local, BorderLayout.NORTH);
		this.add(this.general, BorderLayout.SOUTH);
		
		this.ajouter_listener();
		this.setVisible(true);
		
	}
	
	public void ajouter_listener(){
		this.init.addActionListener(this);
	}

	public void setTab(Vector<tab_stat> vp){
		this.vp=vp;
	}
	
	public Component creer_diag(int nb_vois) {
		this.local.remove(1);
		DefaultPieDataset union = new DefaultPieDataset();
		for (int i = 0; i < vp.size(); i++) {
			//union.setValue(key, value)
			union.setValue(this.vp.elementAt(i).getLettre(), this.vp.elementAt(i).getPercent());
		}
		JFreeChart repart = ChartFactory.createPieChart3D(RdC.elementsTraduction[61]+nb_vois+RdC.elementsTraduction[62], union,
				true, true, false);
		ChartPanel crepart = new ChartPanel(repart);
		crepart.setPreferredSize(new Dimension(300, 200));
		Plot plot = repart.getPlot();
		for (int i = 0; i < this.vp.size(); i++) {
			((PiePlot) plot).setSectionPaint(this.vp.elementAt(i).getLettre(), this.setCouleur());
		}
		this.local.add(crepart);
		return crepart;
	}

	public Color setCouleur(){

		Random r = new Random();
		int v =  r.nextInt(255);
		int b= r.nextInt(255);
		int j= r.nextInt(255);
		Color couleur=new Color(v,b,j);
		return couleur;
	}
	
	public void lecture(String url) {
		BufferedReader lecteur = null;
		String ligne;
		File f = new File(url);
		int comp=1;
		if (f.exists()) {
			try {
				lecteur = new BufferedReader(new FileReader(url));
				while ((ligne = lecteur.readLine()) != null) {
					if (ligne.length() > 0) {
						if(comp==1){
							this.nb_essais=Integer.parseInt(ligne);
						}
						if(comp==2){
							this.nb_reussites=Integer.parseInt(ligne);
						}
						if(comp==3){
							this.nb_erreurs=Integer.parseInt(ligne);
						}
						comp++;
					}
				}
				lecteur.close();
			} catch (FileNotFoundException exc) {
				System.out.println(RdC.elementsTraduction[63]);
			} catch (IOException e) {
				System.out.println(RdC.elementsTraduction[64]);
			}
		}
		//System.out.println("essai: "+this.nb_essais+" reuss: "+this.nb_reussites+" err: "+this.nb_erreurs);
	}

	public void ecriture(String url) {
		PrintWriter ecrivain;
		try {
			ecrivain = new PrintWriter(new BufferedWriter(new FileWriter(url)));
			ecrivain.print(this.nb_essais+"\n");
			ecrivain.print(this.nb_reussites+"\n");
			ecrivain.print(this.nb_erreurs+"\n");
			ecrivain.close();
		} catch (IOException e) {
			System.out.println(RdC.elementsTraduction[64]);
		}
	}
	
	public void init_file(String url){
		PrintWriter ecrivain;
		try {
			ecrivain = new PrintWriter(new BufferedWriter(new FileWriter(url)));
			ecrivain.print("0\n");
			ecrivain.print("0\n");
			ecrivain.print("0\n");
			ecrivain.println();
			ecrivain.close();
		} catch (IOException e) {
			System.out.println(RdC.elementsTraduction[64]);
		}
	}
	
	public void ecrire_lab(){
		this.lab_essais.setText(RdC.elementsTraduction[56]+this.nb_essais);
		this.lab_reuss.setText(RdC.elementsTraduction[57]+this.nb_reussites);
		this.lab_err.setText(RdC.elementsTraduction[58]+this.nb_erreurs);
		if(this.getFiabilite().equals(RdC.elementsTraduction[59])){
			this.fiabilite.setText(RdC.elementsTraduction[60]+this.getFiabilite());
		}else{
			this.fiabilite.setText(RdC.elementsTraduction[60]+this.getFiabilite()+"%");
		}
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==this.init){
			this.init_file("statsgen");
			this.nb_erreurs=0;
			this.nb_reussites=0;
			this.nb_essais=0;
			this.ecrire_lab();
		}
	}
	public void setReussit(){
		this.nb_reussites++;
		this.ecrire_lab();
	}
	public void setErreur(){
		this.nb_erreurs++;
		this.ecrire_lab();
	}
	public void setEssais(){
		this.nb_essais++;
		this.ecrire_lab();
		this.ecriture("statsgen");
	}
	public String getFiabilite(){
		if(this.nb_essais==0){
			return RdC.elementsTraduction[59];
		}
		double fia=(double)((double)this.nb_reussites/(double)this.nb_essais)*100;
		String masque = new String("#0");
		DecimalFormat form = new DecimalFormat(masque); 
		return form.format(fia);
	}
}
