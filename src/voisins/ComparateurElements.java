package voisins;

import java.util.Comparator;
import java.util.Vector;

import baseApp.*;

public class ComparateurElements implements Comparator<Element>{

	distanceEdition.DistanceEdition de;
	Vector<Short> nouveaux;
	
	public ComparateurElements(distanceEdition.DistanceEdition de,Vector<Short> nouveau) {
		this.de = de;
		this.nouveaux=nouveau;
	}

	public int compare(Element o1, Element o2) {
		int d1=de.distance(nouveaux, o1.getContour());
		int d2=de.distance(nouveaux, o2.getContour());
		if(d1<d2){
			//System.out.println("inferieur");
			return -1;
		}
		if(d1>d2){
			//System.out.println("superieur");
			return 1;
		}
		//System.out.println("egalit�");
		return -1;//PAS D'egalitee pour que l'ajout dans l'arbre soit correct !!!!
	}

	
}
