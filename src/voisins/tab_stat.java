package voisins;

public class tab_stat {
	
	double percent;
	Character lettre;
	
	public tab_stat(double percent, Character lettre) {
		this.percent = percent;
		this.lettre = lettre;
	}
	
	public double getPercent(){
		return percent;
	}
	
	public Character getLettre(){
		return lettre;
	}
	
	
}
