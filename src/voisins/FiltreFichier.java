package voisins;

import java.io.File;

import javax.swing.filechooser.FileFilter;

import rdc.RdC;


public class FiltreFichier extends FileFilter {
	public static final String extension = ".ocr";
	@Override
	public boolean accept(File f) {
		String s = (f.toString());
		return (s.endsWith(extension));
			
	}

	@Override
	public String getDescription() {
		return RdC.elementsTraduction[3]+" "+extension;
	}

}
