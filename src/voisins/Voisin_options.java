package voisins;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import rdc.RdC;
import baseApp.BaseApprentissage;
import distanceEdition.DistanceEdition;

public class Voisin_options extends JPanel implements ActionListener,ChangeListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	JCheckBox voisinV1;
	JCheckBox voisinV2;
	JButton changer;
	JTextArea choix_algo=new JTextArea(RdC.elementsTraduction[65]);
	JTextArea choix_K=new JTextArea(RdC.elementsTraduction[67]);
	JTextArea amelioration=new JTextArea(RdC.elementsTraduction[66]);
	JSpinner K;
	JPanel entrerK,choix_alg,mep,mep2,mep3;
	BaseApprentissage ba;
	DistanceEdition de;
	ProchesVoisins pv;
	
	public Voisin_options(DistanceEdition de,BaseApprentissage ba,ProchesVoisins pv) {
		this.ba=ba;
		this.de=de;
		this.pv=pv;
		
		this.setLayout(new GridLayout(6,1));
		this.changer=new JButton(RdC.elementsTraduction[68]);
		this.voisinV1 =new JCheckBox(RdC.elementsTraduction[69]+" 2");
		this.voisinV2 = new JCheckBox(RdC.elementsTraduction[69]+" 1");
		this.disable_text_area();
		this.entrerK=new JPanel();
		this.entrerK.setLayout(new GridLayout(1,2));
		this.choix_alg=new JPanel();
		this.choix_alg.setLayout(new GridLayout(1,2));
		this.mep=new JPanel();
		this.mep.setLayout(new GridLayout(3,3));
		this.mep2=new JPanel();
		this.mep2.setLayout(new GridLayout(3,1));
		this.mep3=new JPanel();
		this.mep3.setLayout(new GridLayout(2,1));
		
		this.K=new JSpinner();
		this.K.setValue((Integer)03);
		
		this.voisinV1.setSelected(true);
		this.voisinV1.setEnabled(false);
		this.voisinV2.setEnabled(false);
		this.entrerK.add(this.choix_K);
		this.mep.add(new JPanel());
		this.mep.add(new JPanel());

		this.mep.add(this.K);
		this.mep.add(new JPanel());
		this.entrerK.add(this.mep);
		
		this.choix_alg.add(this.choix_algo);
		this.mep2.add(this.voisinV1);
		
		this.mep2.add(this.voisinV2);
		this.mep2.add(this.changer);
		this.choix_alg.add(this.mep2);

		
		this.add(this.entrerK);
		this.add(new JPanel());
		
		this.add(this.choix_alg);
		this.add(new JPanel());
		
		this.add_listener();
		this.setVisible(true);
	}

	private void disable_text_area() {
		this.amelioration.setEditable(false);
		this.choix_algo.setEditable(false);
		this.choix_K.setEditable(false);
	}

	private void add_listener() {
		this.changer.addActionListener(this);
		this.K.addChangeListener(this);
	}
	


	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==changer){
			if(voisinV1.isSelected()==true){
				this.voisinV2.setSelected(true);
				this.voisinV1.setSelected(false);
				
			}else{
				this.voisinV1.setSelected(true);
				this.voisinV2.setSelected(false);
			}
		}

	}

	public void stateChanged(ChangeEvent e) {
		if(e.getSource()==K){
			if((Integer)this.K.getValue()<1){
				this.K.setValue((Integer)1);
			}
			if((Integer)this.K.getValue()>30){
				this.K.setValue((Integer)30);
			}
		}
		
	}
	
	
	public boolean getV1(){
		return this.voisinV1.isSelected();
	}
	
	public int getK(){
		return (Integer)this.K.getValue();
	}
	
	public void setK(int k){
		this.K.setValue(k);
	}
	

}
