package voisins;

import java.util.Hashtable;


public class ImageLettre {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Hashtable<Character, String> tab_img;

	public ImageLettre() {
		this.tab_img = init_tab();
	}

	private Hashtable<Character, String> init_tab() {
		this.tab_img = new Hashtable<Character, String>();
		this.tab_img.put('0',
		"./Alphabet/image0.jpg");
		this.tab_img.put('A',
		"./Alphabet/image1.jpg");
		this.tab_img.put('B',
		"./Alphabet/image2.jpg");
		this.tab_img.put('C',
		"./Alphabet/image3.jpg");
		this.tab_img.put('D',
		"./Alphabet/image4.jpg");
		this.tab_img.put('E',
		"./Alphabet/image5.jpg");
		this.tab_img.put('F',
		"./Alphabet/image6.jpg");
		this.tab_img.put('G',
		"./Alphabet/image7.jpg");
		this.tab_img.put('H',
		"./Alphabet/image8.jpg");
		this.tab_img.put('I',
		"./Alphabet/image9.jpg");
		this.tab_img.put('J',
		"./Alphabet/image10.jpg");
		this.tab_img.put('K',
		"./Alphabet/image11.jpg");
		this.tab_img.put('L',
		"./Alphabet/image12.jpg");
		this.tab_img.put('M',
		"./Alphabet/image13.jpg");
		this.tab_img.put('N',
		"./Alphabet/image14.jpg");
		this.tab_img.put('O',
		"./Alphabet/image15.jpg");
		this.tab_img.put('P',
		"./Alphabet/image16.jpg");
		this.tab_img.put('Q',
		"./Alphabet/image17.jpg");
		this.tab_img.put('R',
		"./Alphabet/image18.jpg");
		this.tab_img.put('S',
		"./Alphabet/image19.jpg");
		this.tab_img.put('T',
		"./Alphabet/image20.jpg");
		this.tab_img.put('U',
		"./Alphabet/image21.jpg");
		this.tab_img.put('V',
		"./Alphabet/image22.jpg");
		this.tab_img.put('W',
		"./Alphabet/image23.jpg");
		this.tab_img.put('X',
		"./Alphabet/image24.jpg");
		this.tab_img.put('Y',
		"./Alphabet/image25.jpg");
		this.tab_img.put('Z',
		"./Alphabet/image26.jpg");
		return this.tab_img;
	}

	public Hashtable<Character, String> getTab_img() {
		return tab_img;
	}

	public void setTab_img(Hashtable<Character, String> tabImg) {
		tab_img = tabImg;
	}
}
