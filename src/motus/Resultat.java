/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package motus;

import java.util.Vector;

import rdc.RdC;

/**
 *
 * @author guillaume
 */
public class Resultat{
        private Vector<Vector <Short>> solution;
        private Vector<String> mot_teste;

        public Resultat(){
            solution=new Vector<Vector <Short>>();
            mot_teste=new Vector<String>();
        }

        public Vector<String> getMot_teste() {
            return mot_teste;
        }

        public void addMot_teste(String mot_teste) {
            this.mot_teste.add(mot_teste);
        }

        public Vector<Vector<Short>> getSolution() {
            return solution;
        }

        public void addSolution(Vector<Short> solution) {
            this.solution.add(solution);
        }

        public void affiche(){
            for(int i=0;i<mot_teste.size();i++)
            {
                System.out.println(RdC.elementsTraduction[82]+mot_teste.get(i));
                System.out.print(RdC.elementsTraduction[83]);
                for(int j=0;j<mot_teste.get(i).length();j++)
                {
                    System.out.print(solution.get(i).get(j));

                }
                System.out.println();
            }
        }

    }
