package motus;

import java.util.Vector;

import rdc.RdC;
import dico.*;

/**
 *
 * @author guillaume
 */
public class Motus {
    private String motCherche;
    private VerificateurOrthographe v;
    private boolean gagne;
    private int nb_test;
    private Resultat result;

    public String getMotCherche() {
        return motCherche;
    }




    public Motus(int difficulte)
    {
        this.v = new VerificateurOrthographe(RdC.nomDico);
        this.motCherche= this.v.random_choose(difficulte);
        this.gagne=false;
        this.nb_test=0;
        result=new Resultat();
    }


      public void Verification_motus(String mot){

            Vector <Short> test= new Vector<Short>();
            short []teste=new short[motCherche.length()];
            int second_test;
            result.addMot_teste(mot);
            //System.out.println(this.v.estBienOrthographie(mot)+" et "+ mot.length()+" et "+motCherche.length()+" et "+motCherche.charAt(0)+" et "+mot.charAt(0));
            if(this.v.estBienOrthographie(mot) && mot.length()==motCherche.length() && mot.charAt(0)==motCherche.charAt(0))
            {
                if(mot.equals(motCherche))
                {
                    this.gagne=true;
                    for(int i=0;i<mot.length();i++)
                    {
                        test.add((short)2);
                    }
                    result.addSolution(test);
                    this.nb_test++;
                }
                else
                {
                    //fonction de vérification d'un mot
                    for(int i=0;i<motCherche.length();i++){
                        if(motCherche.charAt(i)==mot.charAt(i))
                        {
                            test.add((short)2);
                            teste[i]=1;
                        }
                        else
                        {
                            test.add((short)0);
                            teste[i]=0;
                        }
                    }
                    for(int i=0;i<motCherche.length();i++){
                        second_test=0;
                        while(second_test<motCherche.length()){
                            if(teste[i]==0 && test.get(second_test)==0 && mot.charAt(i)==motCherche.charAt(second_test)){
                                test.set(i, (short)1);
                                teste[second_test]=1;
                                second_test=motCherche.length();
                            }
                            second_test++;
                        }
                    }
                    result.addSolution(test);
                    this.nb_test++;
                }
            }
            else
            {
                System.out.println(RdC.elementsTraduction[81]);
                for(int i=0;i<mot.length();i++)
                {
                    test.add((short)0);
                }
                result.addSolution(test);
                nb_test++;
            }
        }

    public int getNb_test() {
        return nb_test;
    }

    public Resultat getResult() {
        return result;
    }



/*/
    public static void main(String[] args){
        Motus motus=new Motus(6);
        motus.Verification_motus("JOUON");
        System.out.println("mot à trouver :"+ motus.motCherche);
        motus.getResult().affiche();

		String mot = "ordinateur";
		System.out.println(v.estBienOrthographie(mot));
	}
*/


	public boolean isGagne() {
		return gagne;
	}

}
