package rdc;

import gestionLangage.LangageEtendu;
import gestionLangage.gestionLangue;

import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class EcouteurListe implements ListSelectionListener {

	public String[] elementsTraduction;
	public JList listeLangueDispo;
	public gestionLangue gl;
	public String[] languesDispo;
	
	public EcouteurListe(String[] elementsTraduction,JList listeLangueDispo,gestionLangue gl,String[] languesDispo) {
		this.elementsTraduction = elementsTraduction;
		this.listeLangueDispo = listeLangueDispo;
		this.gl = gl;
		this.languesDispo = languesDispo;
		
	}

	
	public void valueChanged(ListSelectionEvent lse) {
		
		int i = this.listeLangueDispo.getSelectedIndex();
		this.elementsTraduction = (new LangageEtendu("./"+this.gl.getLanguesDisponibles()[i]+".xml")).getElementTraduction();
		RdC.elementsTraduction = this.elementsTraduction;
		RdC.actualise();
	}

}
