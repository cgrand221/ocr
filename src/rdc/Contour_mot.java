package rdc;

import java.util.Collections;
import java.util.Vector;


/**
 *
 * @author guillaume
 */
public class Contour_mot {

    public class struct_lettre implements Comparable<struct_lettre>{
        public Vector<Short> lettre;
        public int position;

        public struct_lettre()
        {	
        	lettre=new Vector<Short>();
        	position=0;
        }
        
        public Vector<Short> getLettre() {
            return lettre;
        }

        public void setLettre(Vector<Short> lettre) {
            this.lettre = lettre;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }
        public void affichelettre(){
            for(int i=0;i<lettre.size();i++)
                System.out.print(lettre.get(i));
        }

        @Override
        public int compareTo(struct_lettre lettre) {
        	return (this.position - lettre.position);
        }
        
        @Override
        public String toString()
        {
        	String affichage=new String();
        	for(int i=0;i<lettre.size();i++)
                affichage+=lettre.get(i);
        	affichage+=RdC.elementsTraduction[38]+position;
        	return affichage;
        }

    }
    private Vector<struct_lettre> result;
    
    public Contour_mot(Vector<Vector<Boolean>> dessin){
        result=new Vector<struct_lettre>();
        Vector<Short> lettre;
        int num_lettre=0;
        int min_x=0,max_x=0;

        short premier_X=0,premier_Y=0;
        while(premier_X!=-1 && premier_Y!=-1)
        {
            lettre=new Vector<Short>();
            premier_X=-1;
            premier_Y=-1;
            for(int i=dessin.size()-1;i>=0 && premier_X==-1;i--)
            {
                //System.err.println("test i="+i);
                for(int j=0;j<dessin.get(i).size() && premier_X==-1;j++)
                {
                    //System.err.println("test j="+j+" value="+dessin.get(i).get(j));
                    if (dessin.get(i).get(j))
                    {
                        premier_X=(short)j;
                        min_x=j;
                        max_x=j;
                        result.add(new struct_lettre());
                        result.get(num_lettre).setPosition(j);
                        premier_Y=(short)i;
                    }
                }
            }
            //System.err.println("sortie ok");
            if(premier_X!=-1 && premier_Y!=-1)
            {
                short sensPrecedent=-1;
                short suivant_X=premier_X,suivant_Y=premier_Y;

                //System.err.println("P_X :"+premier_X+" P_Y :"+premier_Y+" taille :"+dessin.size());
                //après avoir trouvé

                if(dessin.get(premier_Y).get(premier_X+1))//sens 2
                {
                     sensPrecedent=2;
                     lettre.add((short)2);
                     suivant_Y=(short)(premier_Y);
                     suivant_X=(short)(premier_X+1);
                }
                else{
                    if((dessin.get(premier_Y-1).get(premier_X+1)))//sens 3
                    {
                           sensPrecedent=3;
                           lettre.add((short)3);
                           suivant_Y=(short)(premier_Y-1);
                           suivant_X=(short)(premier_X+1);
                    }
                    else
                    {
                       if((dessin.get(premier_Y-1).get(premier_X)))//sens 4
                       {
                             sensPrecedent=4;
                             lettre.add((short)4);
                             suivant_Y=(short)(premier_Y-1);
                             suivant_X=(short)(premier_X);
                        }
                        else
                        {
                               if((dessin.get(premier_Y-1).get(premier_X-1)))//sens 5
                               {
                                   sensPrecedent=5;
                                   lettre.add((short)5);
                                   suivant_Y=(short)(premier_Y-1);
                                   suivant_X=(short)(premier_X-1);
                               }
                        }
                    }
                }
                //System.err.println("P_X :"+premier_X+" P_Y :"+premier_Y);
                //System.err.println(lettre.get(0));

                while(suivant_Y!=premier_Y || suivant_X!=premier_X)
                //boucle qui gère le contour
                //refait 7
                {
                    if(min_x>suivant_X)
                        min_x=suivant_X;
                    if(max_x<suivant_X)
                        max_x=suivant_X;
                    switch (sensPrecedent)
                    {
                        case 0:
                        {
                                    if(dessin.get(suivant_Y+1).get(suivant_X-1))//sens 7
                                    {
                                        sensPrecedent=7;
                                        lettre.add((short)7);
                                        suivant_Y=(short)(suivant_Y+1);
                                        suivant_X=(short)(suivant_X-1);
                                    }
                            else{
                                if((dessin.get(suivant_Y+1).get(suivant_X)))//sens 0
                                {
                                    sensPrecedent=0;
                                    lettre.add((short)0);
                                    suivant_Y=(short)(suivant_Y+1);
                                    suivant_X=(short)(suivant_X);
                                }
                                else
                                {
                                    if((dessin.get(suivant_Y+1).get(suivant_X+1)))//sens 1
                                    {
                                        sensPrecedent=1;
                                        lettre.add((short)1);
                                        suivant_Y=(short)(suivant_Y+1);
                                        suivant_X=(short)(suivant_X+1);
                                    }
                                    else
                                    {
                                        if((dessin.get(suivant_Y).get(suivant_X+1)))//sens 2
                                        {
                                            sensPrecedent=2;
                                            lettre.add((short)2);
                                            suivant_Y=(short)(suivant_Y);
                                            suivant_X=(short)(suivant_X+1);
                                        }
                                        else
                                        {
                                            if((dessin.get(suivant_Y-1).get(suivant_X+1)))//sens 3
                                            {
                                                sensPrecedent=3;
                                                lettre.add((short)3);
                                                suivant_Y=(short)(suivant_Y-1);
                                                suivant_X=(short)(suivant_X+1);
                                            }
                                        }
                                    }
                                }
                            }

                            break;
                        }
                        case 1:
                        {
                                    if(dessin.get(suivant_Y+1).get(suivant_X-1))//sens 7
                                    {
                                        sensPrecedent=7;
                                        lettre.add((short)7);
                                        suivant_Y=(short)(suivant_Y+1);
                                        suivant_X=(short)(suivant_X-1);
                                    }
                            else{
                                if((dessin.get(suivant_Y+1).get(suivant_X)))//sens 0
                                {
                                    sensPrecedent=0;
                                    lettre.add((short)0);
                                    suivant_Y=(short)(suivant_Y+1);
                                    suivant_X=(short)(suivant_X);
                                }
                                else
                                {
                                    if((dessin.get(suivant_Y+1).get(suivant_X+1)))//sens 1
                                    {
                                        sensPrecedent=1;
                                        lettre.add((short)1);
                                        suivant_Y=(short)(suivant_Y+1);
                                        suivant_X=(short)(suivant_X+1);
                                    }
                                    else
                                    {
                                        if((dessin.get(suivant_Y).get(suivant_X+1)))//sens 2
                                        {
                                            sensPrecedent=2;
                                            lettre.add((short)2);
                                            suivant_Y=(short)(suivant_Y);
                                            suivant_X=(short)(suivant_X+1);
                                        }
                                        else
                                        {
                                            if((dessin.get(suivant_Y-1).get(suivant_X+1)))//sens 3
                                            {
                                                sensPrecedent=3;
                                                lettre.add((short)3);
                                                suivant_Y=(short)(suivant_Y-1);
                                                suivant_X=(short)(suivant_X+1);
                                            }
                                            else
                                            {
                                                if((dessin.get(suivant_Y-1).get(suivant_X)))//sens 4
                                                {
                                                    sensPrecedent=4;
                                                    lettre.add((short)4);
                                                    suivant_Y=(short)(suivant_Y-1);
                                                    suivant_X=(short)(suivant_X);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            break;
                        }
                        case 2:
                        {
                            if(dessin.get(suivant_Y+1).get(suivant_X+1))//sens 1
                            {
                                sensPrecedent=1;
                                lettre.add((short)1);
                                suivant_Y=(short)(suivant_Y+1);
                                suivant_X=(short)(suivant_X+1);
                            }
                            else{
                                if((dessin.get(suivant_Y).get(suivant_X+1)))//sens 2
                                {
                                    sensPrecedent=2;
                                    lettre.add((short)2);
                                    suivant_Y=(short)(suivant_Y);
                                    suivant_X=(short)(suivant_X+1);
                                }
                                else
                                {
                                    if((dessin.get(suivant_Y-1).get(suivant_X+1)))//sens 3
                                    {
                                        sensPrecedent=3;
                                        lettre.add((short)3);
                                        suivant_Y=(short)(suivant_Y-1);
                                        suivant_X=(short)(suivant_X+1);
                                    }
                                    else
                                    {
                                        if((dessin.get(suivant_Y-1).get(suivant_X)))//sens 4
                                        {
                                            sensPrecedent=4;
                                            lettre.add((short)4);
                                            suivant_Y=(short)(suivant_Y-1);
                                            suivant_X=(short)(suivant_X);
                                        }
                                        else
                                        {
                                            if((dessin.get(suivant_Y-1).get(suivant_X-1)))//sens 5
                                            {
                                                sensPrecedent=5;
                                                lettre.add((short)5);
                                                suivant_Y=(short)(suivant_Y-1);
                                                suivant_X=(short)(suivant_X-1);
                                            }
                                        }
                                    }
                                }
                            }

                            break;
                        }
                        case 3:
                        {
                            if(dessin.get(suivant_Y+1).get(suivant_X+1))//sens 1
                            {
                                sensPrecedent=1;
                                lettre.add((short)1);
                                suivant_Y=(short)(suivant_Y+1);
                                suivant_X=(short)(suivant_X+1);
                            }
                            else{
                                if((dessin.get(suivant_Y).get(suivant_X+1)))//sens 2
                                {
                                    sensPrecedent=2;
                                    lettre.add((short)2);
                                    suivant_Y=(short)(suivant_Y);
                                    suivant_X=(short)(suivant_X+1);
                                }
                                else
                                {
                                    if((dessin.get(suivant_Y-1).get(suivant_X+1)))//sens 3
                                    {
                                        sensPrecedent=3;
                                        lettre.add((short)3);
                                        suivant_Y=(short)(suivant_Y-1);
                                        suivant_X=(short)(suivant_X+1);
                                    }
                                    else
                                    {
                                        if((dessin.get(suivant_Y-1).get(suivant_X)))//sens 4
                                        {
                                            sensPrecedent=4;
                                            lettre.add((short)4);
                                            suivant_Y=(short)(suivant_Y-1);
                                            suivant_X=(short)(suivant_X);
                                        }
                                        else
                                        {
                                            if((dessin.get(suivant_Y-1).get(suivant_X-1)))//sens 5
                                            {
                                                sensPrecedent=5;
                                                lettre.add((short)5);
                                                suivant_Y=(short)(suivant_Y-1);
                                                suivant_X=(short)(suivant_X-1);
                                            }
                                            else
                                            {
                                                if((dessin.get(suivant_Y).get(suivant_X-1)))//sens 6
                                                {
                                                    sensPrecedent=6;
                                                    lettre.add((short)6);
                                                    suivant_Y=(short)(suivant_Y);
                                                    suivant_X=(short)(suivant_X-1);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        }
                        case 4:
                        {
                            if(dessin.get(suivant_Y-1).get(suivant_X+1))//sens 3
                            {
                                sensPrecedent=3;
                                lettre.add((short)3);
                                suivant_Y=(short)(suivant_Y-1);
                                suivant_X=(short)(suivant_X+1);
                            }
                            else{
                                if((dessin.get(suivant_Y-1).get(suivant_X)))//sens 4
                                {
                                    sensPrecedent=4;
                                    lettre.add((short)4);
                                    suivant_Y=(short)(suivant_Y-1);
                                    suivant_X=(short)(suivant_X);
                                }
                                else
                                {
                                    if((dessin.get(suivant_Y-1).get(suivant_X-1)))//sens 5
                                    {
                                        sensPrecedent=5;
                                        lettre.add((short)5);
                                        suivant_Y=(short)(suivant_Y-1);
                                        suivant_X=(short)(suivant_X-1);
                                    }
                                    else
                                    {
                                        if((dessin.get(suivant_Y).get(suivant_X-1)))//sens 6
                                        {
                                            sensPrecedent=6;
                                            lettre.add((short)6);
                                            suivant_Y=(short)(suivant_Y);
                                            suivant_X=(short)(suivant_X-1);
                                        }
                                        else
                                        {
                                                    if((dessin.get(suivant_Y+1).get(suivant_X-1)))//sens 7
                                                    {
                                                        sensPrecedent=7;
                                                        lettre.add((short)7);
                                                        suivant_Y=(short)(suivant_Y+1);
                                                        suivant_X=(short)(suivant_X-1);
                                                    }
                                        }
                                    }
                                }
                            }
                            break;
                        }
                        case 5:
                        {
                            if(dessin.get(suivant_Y-1).get(suivant_X+1))//sens 3
                            {
                                sensPrecedent=3;
                                lettre.add((short)3);
                                suivant_Y=(short)(suivant_Y-1);
                                suivant_X=(short)(suivant_X+1);
                            }
                            else{
                                if((dessin.get(suivant_Y-1).get(suivant_X)))//sens 4
                                {
                                    sensPrecedent=4;
                                    lettre.add((short)4);
                                    suivant_Y=(short)(suivant_Y-1);
                                    suivant_X=(short)(suivant_X);
                                }
                                else
                                {
                                    if((dessin.get(suivant_Y-1).get(suivant_X-1)))//sens 5
                                    {
                                        sensPrecedent=5;
                                        lettre.add((short)5);
                                        suivant_Y=(short)(suivant_Y-1);
                                        suivant_X=(short)(suivant_X-1);
                                    }
                                    else
                                    {
                                        if((dessin.get(suivant_Y).get(suivant_X-1)))//sens 6
                                        {
                                            sensPrecedent=6;
                                            lettre.add((short)6);
                                            suivant_Y=(short)(suivant_Y);
                                            suivant_X=(short)(suivant_X-1);
                                        }
                                        else
                                        {
                                                    if((dessin.get(suivant_Y+1).get(suivant_X-1)))//sens 7
                                                    {
                                                        sensPrecedent=7;
                                                        lettre.add((short)7);
                                                        suivant_Y=(short)(suivant_Y+1);
                                                        suivant_X=(short)(suivant_X-1);
                                            }
                                            else
                                            {
                                                if((dessin.get(suivant_Y+1).get(suivant_X)))//sens 0
                                                {
                                                    sensPrecedent=0;
                                                    lettre.add((short)0);
                                                    suivant_Y=(short)(suivant_Y+1);
                                                    suivant_X=(short)(suivant_X);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        }
                        case 6:
                        {
                            if(dessin.get(suivant_Y-1).get(suivant_X-1))//sens 5
                            {
                                sensPrecedent=5;
                                lettre.add((short)5);
                                suivant_Y=(short)(suivant_Y-1);
                                suivant_X=(short)(suivant_X-1);
                            }
                            else{
                                if((dessin.get(suivant_Y).get(suivant_X-1)))//sens 6
                                {
                                    sensPrecedent=6;
                                    lettre.add((short)6);
                                    suivant_Y=(short)(suivant_Y);
                                    suivant_X=(short)(suivant_X-1);
                                }
                                else
                                {
                                            if((dessin.get(suivant_Y+1).get(suivant_X-1)))//sens 7
                                            {
                                                sensPrecedent=7;
                                                lettre.add((short)7);
                                                suivant_Y=(short)(suivant_Y+1);
                                                suivant_X=(short)(suivant_X-1);
                                    }
                                    else
                                    {
                                        if((dessin.get(suivant_Y+1).get(suivant_X)))//sens 0
                                        {
                                            sensPrecedent=0;
                                            lettre.add((short)0);
                                            suivant_Y=(short)(suivant_Y+1);
                                            suivant_X=(short)(suivant_X);
                                        }
                                        else
                                        {
                                            if((dessin.get(suivant_Y+1).get(suivant_X+1)))//sens 1
                                            {
                                                sensPrecedent=1;
                                                lettre.add((short)1);
                                                suivant_Y=(short)(suivant_Y+1);
                                                suivant_X=(short)(suivant_X+1);
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        }
                        case 7:
                        {
                            if(dessin.get(suivant_Y-1).get(suivant_X-1))//sens 5
                            {
                                sensPrecedent=5;
                                lettre.add((short)5);
                                suivant_Y=(short)(suivant_Y-1);
                                suivant_X=(short)(suivant_X-1);
                            }
                            else{
                                if((dessin.get(suivant_Y).get(suivant_X-1)))//sens 6
                                {
                                    sensPrecedent=6;
                                    lettre.add((short)6);
                                    suivant_Y=(short)(suivant_Y);
                                    suivant_X=(short)(suivant_X-1);
                                }
                                else
                                {
                                            if((dessin.get(suivant_Y+1).get(suivant_X-1)))//sens 7
                                            {
                                                sensPrecedent=7;
                                                lettre.add((short)7);
                                                suivant_Y=(short)(suivant_Y+1);
                                                suivant_X=(short)(suivant_X-1);
                                    }
                                    else
                                    {
                                        if((dessin.get(suivant_Y+1).get(suivant_X)))//sens 0
                                        {
                                            sensPrecedent=0;
                                            lettre.add((short)0);
                                            suivant_Y=(short)(suivant_Y+1);
                                            suivant_X=(short)(suivant_X);
                                        }
                                        else
                                        {
                                            if((dessin.get(suivant_Y+1).get(suivant_X+1)))//sens 1
                                            {
                                                sensPrecedent=1;
                                                lettre.add((short)1);
                                                suivant_Y=(short)(suivant_Y+1);
                                                suivant_X=(short)(suivant_X+1);
                                            }
                                            else
                                            {
                                                if((dessin.get(suivant_Y).get(suivant_X+1)))//sens 2
                                                {
                                                    sensPrecedent=2;
                                                    lettre.add((short)2);
                                                    suivant_Y=(short)(suivant_Y);
                                                    suivant_X=(short)(suivant_X+1);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        }
                        default :
                        {
                            break;
                        }
                    }
                }
            	for(int l=0;l<dessin.size();l++)
	                for(int h=min_x; h<=max_x;h++)
	                		dessin.get(l).setElementAt(false, h);
	                
                result.get(num_lettre).setLettre(lettre);
                num_lettre++;
            }
        }
        Collections.sort(result);
    }


    public void afficheResult(){
        for(int i=0;i<result.size();i++)
            System.out.println(result.get(i));
    }

    public Vector<struct_lettre> getResult() {
        return result;
    }

}
