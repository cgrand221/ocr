package rdc;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Vector;

import javax.swing.JColorChooser;
import javax.swing.JPanel;


public class Panneau extends JPanel implements MouseMotionListener,MouseListener
{
	public static Image pencil = Toolkit.getDefaultToolkit().getImage("./images/pencil.gif");
	public static Cursor crayon = Toolkit.getDefaultToolkit().createCustomCursor(pencil, new Point(0, 0), "crayon"/*RdC.elementsTraduction[32]*/);
	public static Image eraser = Toolkit.getDefaultToolkit().getImage("./images/eraser.gif");
	public static Cursor gomme = Toolkit.getDefaultToolkit().createCustomCursor(eraser, new Point(0, 0), "gomme"/*RdC.elementsTraduction[33]*/);
	private static final long serialVersionUID = 420;
	private static final double EPSILON = 0.1;
	int xPrec, yPrec;
	int LONG = 120;
	int HAUT = 150;
	int DIV = 6;
	Color couleur = Color.BLACK;
	Vector<Vector <Boolean>> matrice = new Vector <Vector <Boolean>> (); //ce qui est code
	Vector<Vector <Boolean>> affichage = new Vector <Vector <Boolean>> (); //ce qui est visualise

	public Panneau()
	{
		//Initialisation du vecteur de matrice
		for(int j = 0;j<HAUT/DIV+4;j++)
		{
			Vector<Boolean> remplissage=new Vector<Boolean>();
			for (int i = 0;i<LONG/DIV+5;i++)
				remplissage.add(false);
			matrice.add(remplissage);
		}

		//Initialisation du vecteur d'affichage
		for(int j = 0;j<HAUT+4;j++)
		{
			Vector<Boolean> remplissage=new Vector<Boolean>();
			for (int i = 0;i<LONG+5;i++)
				remplissage.add(false);
			affichage.add(remplissage);
		}

		addMouseMotionListener(this);
		addMouseListener(this);
		this.setSize(LONG, HAUT);
		//System.out.println(LONG+";"+HAUT);
		this.setBackground(Color.white);
		this.setCursor(crayon);
	}

	public Vector<Vector <Boolean>> getMatrice()
	{
		return this.matrice;
	}
	
	public void setMatrice(Vector<Vector <Boolean>> matrice)
	{
		this.matrice = matrice;
	}

	public void mettrePixel(int x,int y)
	{
		//dessine le pixel en (x,y) en prenant en compte le decalage de la matrice et la largeur de 3 du pinceau
		matrice.elementAt(x/DIV+1).setElementAt(true, y/DIV+3);
		matrice.elementAt(x/DIV+1).setElementAt(true, y/DIV+2);
		matrice.elementAt(x/DIV+1).setElementAt(true, y/DIV+1);
		matrice.elementAt(x/DIV+2).setElementAt(true, y/DIV+3);
		matrice.elementAt(x/DIV+2).setElementAt(true, y/DIV+2);
		matrice.elementAt(x/DIV+2).setElementAt(true, y/DIV+1);
		matrice.elementAt(x/DIV+3).setElementAt(true, y/DIV+3);
		matrice.elementAt(x/DIV+3).setElementAt(true, y/DIV+2);
		matrice.elementAt(x/DIV+3).setElementAt(true, y/DIV+1);


		//dessine le pixel en (x,y) en prenant en compte le decalage de la matrice et la largeur de 3 du pinceau
		affichage.elementAt(x+1).setElementAt(true, y+3);
		affichage.elementAt(x+1).setElementAt(true, y+2);
		affichage.elementAt(x+1).setElementAt(true, y+1);
		affichage.elementAt(x+2).setElementAt(true, y+3);
		affichage.elementAt(x+2).setElementAt(true, y+2);
		affichage.elementAt(x+2).setElementAt(true, y+1);
		affichage.elementAt(x+3).setElementAt(true, y+3);
		affichage.elementAt(x+3).setElementAt(true, y+2);
		affichage.elementAt(x+3).setElementAt(true, y+1);
	}

	public void enleverPixel(int x,int y)
	{
		//dessine le pixel en (x,y) en prenant en compte le decalage de la matrice et la largeur de 3 du pinceau
		matrice.elementAt(x/DIV+1).setElementAt(false, y/DIV+3);
		matrice.elementAt(x/DIV+1).setElementAt(false, y/DIV+2);
		matrice.elementAt(x/DIV+1).setElementAt(false, y/DIV+1);
		matrice.elementAt(x/DIV+2).setElementAt(false, y/DIV+3);
		matrice.elementAt(x/DIV+2).setElementAt(false, y/DIV+2);
		matrice.elementAt(x/DIV+2).setElementAt(false, y/DIV+1);
		matrice.elementAt(x/DIV+3).setElementAt(false, y/DIV+3);
		matrice.elementAt(x/DIV+3).setElementAt(false, y/DIV+2);
		matrice.elementAt(x/DIV+3).setElementAt(false, y/DIV+1);


		//dessine le pixel en (x,y) en prenant en compte le decalage de la matrice et la largeur de 3 du pinceau
		affichage.elementAt(x+1).setElementAt(false, y+3);
		affichage.elementAt(x+1).setElementAt(false, y+2);
		affichage.elementAt(x+1).setElementAt(false, y+1);
		affichage.elementAt(x+2).setElementAt(false, y+3);
		affichage.elementAt(x+2).setElementAt(false, y+2);
		affichage.elementAt(x+2).setElementAt(false, y+1);
		affichage.elementAt(x+3).setElementAt(false, y+3);
		affichage.elementAt(x+3).setElementAt(false, y+2);
		affichage.elementAt(x+3).setElementAt(false, y+1);
	}


	//evenement deplacement souris avec bouton enfonce
	public void mouseDragged(MouseEvent e)
	{
		Graphics g = getGraphics();
		g.setColor(couleur);

		/*
		if((e.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK)==InputEvent.BUTTON3_DOWN_MASK)
		{
			// Bouton DROIT presse
			this.setCursor(gomme);
		}
		 */

		int x = e.getX();
		int y = e.getY();
		int a,b,i,c,d,k;
		float j;
		if (x<1)
			x = 1;
		if (y<1)
			y=1;
		if (x>LONG-2)
			x = LONG-2;
		if (y>HAUT-2)
			y = HAUT-2;

		if (xPrec > x)
		{
			a = x;
			c = y;
			b = xPrec;
			d = yPrec;
		}
		else
		{
			b = x;
			d = y;
			a = xPrec;
			c = yPrec;
		}
		//System.out.println("a="+a+" b="+b+" c="+c+" d="+d);
		if (Math.abs(x-xPrec) < EPSILON)
		{
			if (d < c)
			{
				i = c;
				c = d;
				d = i;
			}
			//c < d
			//a < b
			for(i = c;i <=d ;i++)
			{
				if((e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK)==InputEvent.BUTTON1_DOWN_MASK)
				{
					// Bouton GAUCHE enfonce
					mettrePixel(i, x);
				}
				if((e.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK)==InputEvent.BUTTON3_DOWN_MASK)
				{
					// Bouton DROIT enfonce
					enleverPixel(i, x);
				}
			}
		}
		else
		{
			double coefDir = ((double) (c-d))/(a-b)*EPSILON;
			j = (float) c;
			double tmp;
			for (tmp = a;tmp <= b;tmp+=EPSILON)
			{
				k = (int) j;
				if (k<1)
					k = 1;
				if (k>HAUT-2)
					k = HAUT-2;

				if((e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK)==InputEvent.BUTTON1_DOWN_MASK)
				{
					// Bouton GAUCHE enfonce
					mettrePixel(k,(int) tmp);
				}
				if((e.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK)==InputEvent.BUTTON3_DOWN_MASK)
				{
					// Bouton DROIT enfonce
					enleverPixel(k,(int) tmp);
				}				
				j+=coefDir;
			}
		}

		if((e.getModifiersEx() & InputEvent.BUTTON1_DOWN_MASK)==InputEvent.BUTTON1_DOWN_MASK)
		{
			// Bouton GAUCHE enfonce
			mettrePixel(y,x);
		}
		if((e.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK)==InputEvent.BUTTON3_DOWN_MASK)
		{
			// Bouton DROIT enfonce
			enleverPixel(y,x);
		}		

		this.repaint();
		//System.out.println("(x="+x+" ; y="+y+")");			
		mouseMoved(e);
	}

	//evenement lors du deplacement de la souris
	public void mouseMoved(MouseEvent e)
	{
		xPrec = e.getX();
		yPrec = e.getY();
		if (xPrec<1)
			xPrec = 1;
		if (yPrec<1)
			yPrec=1;
		if (xPrec>LONG-2)
			xPrec = LONG-2;
		if (yPrec>HAUT-2)
			yPrec = HAUT-2;
		//System.out.println("(x="+xPrec+" ; y="+yPrec+")");	
	}

	protected void paintComponent(Graphics g)
	{	
		super.paintComponent(g);
		g.setColor(couleur);
		for (int i = 1;i<HAUT+3;i++)
		{
			for(int j = 1;j<LONG+4;j++)
			{
				if(affichage.elementAt(i).elementAt(j))
				{
					g.drawRect(j-2, i-2, 0, 0);
				}
			}
		}
	}
	//methode de changement de couleur de dessin
	//utilisation de la boite de dialogue JColorChooser
	public void changeCouleur()
	{
		couleur = JColorChooser.showDialog(this, RdC.elementsTraduction[31], couleur);
	}

	//methode pour effacer le dessin
	public void efface()
	{
		/*
		//Boucles pour afficher le vecteur
		for (int i = HAUT/DIV+3;i>=0;i--)
		{			
			for(int j = 0;j<LONG/DIV+4;j++)
			{
				if(matrice.elementAt(i).elementAt(j))
					System.out.print(0);
				else 
					System.out.print(1);
			}
			System.out.println();
		}
		System.out.println();
		 */

		//Boucles pour reinitialiser le vecteur
		for (int i = 0;i<HAUT/DIV+4;i++)
		{			
			for(int j = 0;j<LONG/DIV+5;j++)
			{
				//matricematrice[i][j] = false;
				matrice.elementAt(i).setElementAt(false, j);
			}
		}
		//Boucles pour reinitialiser le vecteur
		for (int i = 0;i<HAUT+4;i++)
		{			
			for(int j = 0;j<LONG+4;j++)
			{
				//matricematrice[i][j] = false;
				affichage.elementAt(i).setElementAt(false, j);
			}
		}
		repaint();
	}
	
	public void mousePressed(MouseEvent e)
	{
		if(((e.getModifiersEx() & InputEvent.BUTTON3_DOWN_MASK))==InputEvent.BUTTON3_DOWN_MASK)
			this.setCursor(gomme);
	}
	public void mouseReleased(MouseEvent arg0)
	{
		this.setCursor(crayon);
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
