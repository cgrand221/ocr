package rdc;

import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Dialogue extends JDialog
{
	private static final long serialVersionUID = 420;
	JLabel message_haut = new JLabel();
	JLabel message_bas = new JLabel();
	JPanel pan = new JPanel();
	JLabel image = new JLabel(new ImageIcon("./images/progress.gif"));

	public Dialogue(JFrame f, String titre, boolean modal)
	{
		super(f,titre, modal);

		try
		{
			Init();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	// l interface
	public Dialogue()
	{
		try
		{
			Init();
		}

		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	private void Init() throws Exception
	{
		message_haut.setText(RdC.elementsTraduction[35]);
		pan.add(image);
		this.getContentPane().add(message_haut, BorderLayout.NORTH);
	    this.getContentPane().add(pan, BorderLayout.CENTER);
	    message_bas.setText(RdC.elementsTraduction[36]);
		this.getContentPane().add(message_bas, BorderLayout.SOUTH);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.pack();
	}
}
