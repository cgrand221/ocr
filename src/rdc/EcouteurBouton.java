package rdc;

import gestionLangage.LangageEtendu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JList;

public class EcouteurBouton implements ActionListener {

	
	private JList langues;
	public JFrame f;
	public EcouteurBouton(JList langues,JFrame f){
		this.f = f;
		this.langues = langues;
	}

	public void actionPerformed(ActionEvent ae) {
		if (this.langues.getSelectedIndex() != -1){
			f.setVisible(false);
			f = null;
			LangageEtendu le = new LangageEtendu(this.langues.getSelectedValue()+".xml");
			RdC.main2(le.getElementTraduction(),le.getNomDico());
		}

	}

}
