/* Programme Principal */
package rdc;

import gestionLangage.LangageEtendu;
import gestionLangage.gestionLangue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Vector;

import distanceEdition.*;
import voisins.*;
import baseApp.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.text.DefaultCaret;

import motus.JMotus;

import rdc.Contour_mot.struct_lettre;



public class RdC
{
	public static String nomDico;
	final static JPanel panneau = new JPanel(new BorderLayout());
	final static JPanel panneau_messages = new JPanel(new BorderLayout());
	public static int LONG = 1024, HAUT = 576;
	public static String credits;
	static JFrame fenetre = new JFrame();
	static JTextArea message =new JTextArea(credits, 3, 15);
	static JScrollPane scroll;
	static JPanel saisie = new JPanel();
	static JPanel saisie_dessin = new JPanel();
	public static Panneau dessin = new Panneau();
	static JPanel commandes = new JPanel(); 
	static JPanel commandes2 = new JPanel(); 
	public static Panneau_Mot dessin_mot = new Panneau_Mot();
	static JPanel panel_mot = new JPanel();
	static JPanel saisie_mot = new JPanel();
	static JPanel commandes_mot = new JPanel();
	static JPanel panel_commandes = new JPanel(); 
	static JTextArea mot_reconnu = new JTextArea(1, 10);
	static JTabbedPane tableOnglet = new JTabbedPane();
	static String mot = "";
	public static DistanceEdition de;
	public static BaseInterface bi;
	public static ProchesVoisins pv;
	public static JMotus jeu;

	private static DefaultCaret caret;
	private static JButton validationLangue;
	private static JList listeLanguesDispo;
	private static JFrame f;

	public static void init()
	{

		fenetre.setSize(LONG, HAUT);
		de.setBaseApprentissage(bi);

		caret = (DefaultCaret) message.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);


		scroll.getVerticalScrollBar().addMouseListener(new MouseListener()
		{
			public void mouseClicked(MouseEvent e) {}
			public void mouseEntered(MouseEvent e) {}
			public void mouseExited(MouseEvent e) {}
			public void mousePressed(MouseEvent e)
			{
				caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);        
			}
			public void mouseReleased(MouseEvent e) {
				if(scroll.getVerticalScrollBar().getMaximum()<=(scroll.getVerticalScrollBar().getValue()+scroll.getVerticalScrollBar().getHeight())) 
					caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
				else caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
			}    
		});

		scroll.addMouseWheelListener(new MouseWheelListener()
		{
			public void mouseWheelMoved(MouseWheelEvent e)
			{
				int notches = e.getWheelRotation();
				if (notches < 0)
				{
					//ici tu scroll vers le haut
					caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);                    
				}
				else
				{
					//ici tu scroll vers le bas
					if(scroll.getVerticalScrollBar().getMaximum()
							<=(scroll.getVerticalScrollBar().getValue()+scroll.getVerticalScrollBar().getHeight()))
					{
						caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
					}
				}
			}
		});
	}

	protected static void menuQuitterMousePressed(MouseEvent evt)
	{
		//On quitte l'application
		System.exit(0);
	}

	public static void reset()
	{
		dessin.efface();
		dessin_mot.efface();
		pv.effacer();
		pv.changeK(3);
		JMotus.Panneau.efface();
		BaseInterface.panneauBase.efface();
		mot_reconnu.setText("");

		dessin.couleur = Color.BLACK;;
		dessin_mot.couleur = Color.BLACK;;
		JMotus.Panneau.couleur = Color.BLACK;

		Toolkit k = Toolkit.getDefaultToolkit();
		Dimension tailleEcran = k.getScreenSize();
		int longeurEcran = tailleEcran.width;
		int hauteurEcran = tailleEcran.height;

		fenetre.setSize(LONG, HAUT);
		fenetre.setLocation(longeurEcran/2-LONG/2, hauteurEcran/2-HAUT/2); //centre l'affichage par rapport a la fenetre
	}




	public static void main (String args[])
	{
		// dans xml &#0010; est le caractere \n
		gestionLangue gl = new gestionLangue("./");
		String[] languesDispo = gl.getLanguesDisponibles();

		LangageEtendu le = new LangageEtendu(languesDispo[0]+".xml");

		String[] elementsTraduction = le.getElementTraduction();
		f = new JFrame(elementsTraduction[0]);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(new Dimension(400,300));
		JPanel panneau = new JPanel();
		panneau.setPreferredSize(new Dimension(190,150));
		panneau.setLayout(new GridLayout(1,1));

		listeLanguesDispo = new JList(languesDispo);
		listeLanguesDispo.addListSelectionListener(new EcouteurListe(elementsTraduction,listeLanguesDispo,gl,languesDispo));
		JScrollPane defilement= new JScrollPane(listeLanguesDispo);


		Toolkit k = Toolkit.getDefaultToolkit();
		Dimension tailleEcran = k.getScreenSize();
		int longeurEcran = tailleEcran.width;
		int hauteurEcran = tailleEcran.height;
		panneau.add(defilement);
		JPanel panneau2 = new JPanel();
		//panneau2.setPreferredSize(new Dimension(500,190));
		//panneau2.setLayout(new GridLayout(2,1));
		panneau2.add(panneau);
		validationLangue = new JButton();
		validationLangue.setText(elementsTraduction[1]);
		validationLangue.addActionListener(new EcouteurBouton(listeLanguesDispo,f));
		panneau2.add(validationLangue);

		f.add(panneau2);
		f.setLocation(longeurEcran/2-400/2, hauteurEcran/2-300/2); //centre l'affichage par rapport a la fenetre
		f.setVisible(true);
	}

	public static void actualise()
	{
		f.setTitle(elementsTraduction[0]);
		validationLangue.setText(elementsTraduction[1]);
	}
	public static String[] elementsTraduction;

	public static void main2(String[] elementsTraduction, String nomDico)
	{
		credits = RdC.elementsTraduction[139]+"\n\tGUNGOR Fatih\n" +
		"\n"+RdC.elementsTraduction[140]+"\n\tBALLAS Florian\n\tDELOMIER Guillaume\n\tGRAND Christopher\n\tJUILLET Guillaume\n\n" +
		"\n"+RdC.elementsTraduction[141]+"Elisa FROMONT "+RdC.elementsTraduction[142]+" Marc SEBBAN.\n" +
		"\n"+RdC.elementsTraduction[143]+" : gungor.f@gmail.com\n\n" +
		"\n\nMaster 1 Web Intelligence - Saint Etienne - "+RdC.elementsTraduction[144]+" 2010.";
		RdC.nomDico = nomDico;
		JMenuBar menu = new JMenuBar ();
		RdC.elementsTraduction = elementsTraduction;
		
		RdC.de = new DistanceEdition();
		RdC.bi = new BaseInterface();
		RdC.pv = new ProchesVoisins(null, bi.ba, de);
		RdC.jeu = new JMotus(pv);
		
		
		//Proprietes du Menu
		menu.setName(elementsTraduction[2]);
		menu.setBackground(null);
		menu.setAutoscrolls(true);
		menu.setBorder(BorderFactory.createEmptyBorder(10,0,0,0));
		menu.setOpaque(true);

		//Creation des rubriques du Menu
		JMenu menuFichier = new JMenu();
		JMenu menuOptions = new JMenu();

		JMenuItem menuQuitter = new JMenuItem();
		JMenuItem menuCouleur = new JMenuItem();
		JMenuItem menuSauvegarder = new JMenuItem();

		//Etiquettes 
		menuFichier.setText(elementsTraduction[3]);
		menuQuitter.setText(elementsTraduction[4]);
		menuOptions.setText(elementsTraduction[5]);
		menuCouleur.setText(elementsTraduction[6]);
		menuSauvegarder.setText(elementsTraduction[7]);

		//Raccourcis
		//menuQuitter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		//menuCouleur.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
		//menuSauvegarder.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));

		//On ajoute les sous rubriques dans les rubriques
		menuFichier.add(menuQuitter);
		menuOptions.add(menuCouleur);
		menuOptions.add(menuSauvegarder);

		//On ajoute les rubriques a la barre de menu
		menu.add(menuFichier);
		menu.add(menuOptions);

		System.out.println(elementsTraduction[8]);

		/*** Onglet Saisie ***/

		saisie_dessin.add(dessin);
		saisie_dessin.setBackground(Color.LIGHT_GRAY);
		dessin.setPreferredSize(new Dimension(120,150));
		commandes.setLayout(new GridLayout(2,1));
		commandes.add(saisie_dessin);

		commandes.add(commandes2);
		JButton btn_reconnaitre = new JButton(elementsTraduction[9]);
		btn_reconnaitre.setToolTipText(elementsTraduction[10]);
		btn_reconnaitre.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				Contour contour = new Contour(dessin.getMatrice());
				Vector<Short> resultat = contour.getResult();
				if(resultat.size()!=0)
				{
					contour.afficheResult();
					pv.setNouvelElement(resultat);
					pv.chercher_voisin();
				}
			}
		}
		);

		JButton btn_effacer = new JButton(elementsTraduction[11]);
		btn_effacer.setToolTipText(elementsTraduction[12]);
		btn_effacer.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				dessin.efface();
			}
		}
		);

		JButton btn_apprendre = new JButton(elementsTraduction[13]);
		btn_apprendre.setToolTipText(elementsTraduction[14]);
		btn_apprendre.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				baseApp.BaseInterface.panneauBase.matrice=dessin.matrice;
				baseApp.BaseInterface.panneauBase.affichage=dessin.affichage;
				baseApp.BaseInterface.panneauBase.repaint();
				int selectedIndex = tableOnglet.getSelectedIndex();
				selectedIndex = (selectedIndex + 3) % tableOnglet.getTabCount();
				tableOnglet.setSelectedIndex(selectedIndex);
			}
		}
		);

		commandes2.add(btn_effacer);
		commandes2.add(btn_reconnaitre);
		commandes2.add(btn_apprendre);

		saisie.setLayout(new GridLayout(1,2));
		saisie.add(commandes);
		saisie.add(pv);
		tableOnglet.addTab(elementsTraduction[15], saisie);
		panneau.add(tableOnglet);

		/*** Onglet Saisie Mot ***/

		saisie_mot.add(dessin_mot);
		saisie_mot.setBackground(Color.LIGHT_GRAY);
		dessin_mot.setPreferredSize(new Dimension(920,150));

		mot_reconnu.setFont(new Font("TimesRoman", Font.PLAIN, 42));

		JButton btn_mot = new JButton(elementsTraduction[9]);
		btn_mot.setToolTipText(elementsTraduction[16]);
		btn_mot.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				mot = "";
				Vector<Vector <Boolean>> temp = new Vector<Vector<Boolean>> ();
				for(int j = 0;j<dessin_mot.getMatrice().size();j++)
				{
					Vector<Boolean> remplissage=new Vector<Boolean>();
					for (int i = 0;i<dessin_mot.getMatrice().get(j).size();i++)
						remplissage.add(dessin_mot.getMatrice().get(j).get(i));
					temp.add(remplissage);
				}

				Contour_mot contour = new Contour_mot(temp);
				Vector<struct_lettre> resultat = contour.getResult();
				if(resultat.size()!=0)
				{
					contour.afficheResult();
					Vector<Short> lettre = new Vector<Short> ();
					for(int i=0;i<resultat.size();i++)
					{
						lettre = resultat.get(i).getLettre();
						pv.setNouvelElement(lettre);
						Vector<Element> car = pv.chercher_voisin();
						mot+=pv.determine_lettre(car);
						System.out.println(pv.determine_lettre(car));
					}
					mot_reconnu.setText(mot);
					System.out.println(RdC.elementsTraduction[17]+mot);
				}
			}
		}
		);

		JButton btn2_mot = new JButton(RdC.elementsTraduction[11]);
		btn2_mot.setToolTipText(RdC.elementsTraduction[12]);
		btn2_mot.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				dessin_mot.efface();
				mot = "";
			}
		}
		);
		commandes_mot.add(btn2_mot);
		commandes_mot.add(btn_mot);

		JLabel lab1=new JLabel(RdC.elementsTraduction[17]);
		lab1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		JButton btn_reset_mot = new JButton(RdC.elementsTraduction[18]);
		btn_reset_mot.setToolTipText(RdC.elementsTraduction[19]);
		btn_reset_mot.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent ae)
			{
				mot_reconnu.setText("");
			}
		}
		);

		panel_commandes.add(lab1);
		panel_commandes.add(mot_reconnu);
		panel_commandes.add(btn_reset_mot);

		panel_mot.setLayout(new BorderLayout());
		panel_mot.add(saisie_mot, BorderLayout.NORTH);
		panel_mot.add(commandes_mot, BorderLayout.CENTER);
		panel_mot.add(panel_commandes, BorderLayout.SOUTH);

		scroll = new JScrollPane(panel_mot);
		panneau.add(scroll);
		tableOnglet.addTab(elementsTraduction[20], scroll);
		panneau.add(tableOnglet);

		/*** Onglet Distance d'Edition ***/

		scroll = new JScrollPane(de);
		panneau.add(scroll);
		tableOnglet.addTab(RdC.elementsTraduction[21], scroll);
		panneau.add(tableOnglet);

		scroll = new JScrollPane(bi);
		panneau.add(scroll);
		tableOnglet.addTab(RdC.elementsTraduction[22], scroll);
		panneau.add(tableOnglet);

		/*** Onglet Jeu ***/

		scroll = new JScrollPane(jeu);
		panneau.add(scroll);
		tableOnglet.addTab(elementsTraduction[23], scroll);
		panneau.add(tableOnglet);

		/*** Onglet Credits ***/

		message.setLineWrap(true);
		message.setWrapStyleWord(true);
		message.setText(credits);
		message.setEditable(false);
		scroll = new JScrollPane(message);
		panneau.add(scroll);
		tableOnglet.addTab(elementsTraduction[24], scroll);
		panneau.add(tableOnglet);

		/*** Fin des Onglets ***/

		//Creation des icones (de gauche a droite dans l'interface)	
		JButton reset = new JButton(new ImageIcon("./images/reset.png"));
		reset.setToolTipText(elementsTraduction[25]);
		JButton exit = new JButton(new ImageIcon("./images/exit.png"));
		exit.setToolTipText(elementsTraduction[26]);

		//Creation de la JToolBar
		final JToolBar barre_outils = new JToolBar();
		//Insertion des icones dans la JToolBar
		barre_outils.add(reset);
		barre_outils.addSeparator();
		barre_outils.add(exit);

		/*** Fenetre Principale ***/		
		panneau.add(barre_outils, BorderLayout.NORTH);
		Toolkit k = Toolkit.getDefaultToolkit();
		Dimension tailleEcran = k.getScreenSize();
		int longeurEcran = tailleEcran.width;
		int hauteurEcran = tailleEcran.height;

		fenetre.setLocation(longeurEcran/2-LONG/2, hauteurEcran/2-HAUT/2); //centre l'affichage par rapport a la resolution de l'ecran
		fenetre.setContentPane(panneau);
		fenetre.setTitle(elementsTraduction[30]);
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.setVisible(true);
		fenetre.setJMenuBar(menu);
		fenetre.setResizable(true);

		//Quand on appuie sur Quitter dans le menu
		menuQuitter.addMouseListener(new java.awt.event.MouseAdapter()
		{
			public void mousePressed(java.awt.event.MouseEvent evt)
			{
				System.out.println(RdC.elementsTraduction[28]);
				menuQuitterMousePressed(evt);
			}
		});

		//Quand on appuie sur Chancer les couleurs dans le menu
		menuCouleur.addMouseListener(new java.awt.event.MouseAdapter()
		{
			public void mousePressed(java.awt.event.MouseEvent evt)
			{
				dessin.changeCouleur();
				dessin_mot.changeCouleur();
				JMotus.Panneau.changeCouleur();
			}
		});

		//Quand on appuie sur Sauvegarder le panel dans le menu
		menuSauvegarder.addMouseListener(new java.awt.event.MouseAdapter()
		{
			public void mousePressed(java.awt.event.MouseEvent evt)
			{
				BufferedImage img = new BufferedImage(dessin.getWidth(), dessin.getHeight(), BufferedImage.TYPE_INT_RGB);
				Graphics2D g2 = img.createGraphics();
				dessin.paint(g2);
				try
				{
					ImageIO.write(img, "png", new File("screen.png"));
					System.out.println(RdC.elementsTraduction[29]);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});

		//Quand on appuie sur le bouton Exit
		exit.addMouseListener(new java.awt.event.MouseAdapter()
		{
			public void mousePressed(java.awt.event.MouseEvent evt)
			{
				System.out.println(RdC.elementsTraduction[28]);
				menuQuitterMousePressed(evt);
			}
		});      

		//Quand on appuie sur le bouton Reset
		reset.addMouseListener(new java.awt.event.MouseAdapter()
		{
			public void mousePressed(java.awt.event.MouseEvent evt)
			{
				reset();
			}
		});
		/*
		File chartefichier = new File("./Charte.txt");
		BufferedReader bf = null;
		String ligne="";
		String charte="";
		try {
			bf = new BufferedReader(new FileReader(chartefichier));
			while((ligne=bf.readLine())!=null)
				charte+="\n"+ligne;
		} catch (Exception e) {
			System.err.println("(Client) Erreur lors de l'ouverture de la Charte");
		}
		JOptionPane.showMessageDialog(fenetre,charte,
				"Charte de surveillance",
				JOptionPane.PLAIN_MESSAGE);
		 */
		init();
		System.out.println(RdC.elementsTraduction[30]);
	}
}


