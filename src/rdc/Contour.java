/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package rdc;

import java.util.Vector;


/**
 *
 * @author guillaume
 */
public class Contour {
	private Vector<Short> result;

	public Contour(Vector<Vector<Boolean>> dessin){
		result=new Vector<Short>();
		short premier_X=-1,premier_Y=-1;
		for(int i=dessin.size()-1;i>=0 && premier_X==-1;i--)
		{
			for(int j=0;j<dessin.get(i).size() && premier_X==-1;j++)
			{
				if (dessin.get(i).get(j))
				{
					premier_X=(short)j;
					premier_Y=(short)i;
				}   
			}
		}
		short sensPrecedent=-1;
		short suivant_X=premier_X,suivant_Y=premier_Y;
		//après avoir trouvé
		if(premier_X!=-1)
		{
			if(dessin.get(premier_Y).get(premier_X+1))//sens 2
			{
				sensPrecedent=2;
				result.add((short)2);
				suivant_Y=(short)(premier_Y);
				suivant_X=(short)(premier_X+1);
			}
			else{       
				if((dessin.get(premier_Y-1).get(premier_X+1)))//sens 3
				{
					sensPrecedent=3;
					result.add((short)3);
					suivant_Y=(short)(premier_Y-1);
					suivant_X=(short)(premier_X+1);
				}
				else
				{
					if((dessin.get(premier_Y-1).get(premier_X)))//sens 4
					{
						sensPrecedent=4;
						result.add((short)4);
						suivant_Y=(short)(premier_Y-1);
						suivant_X=(short)(premier_X);
					}
					else
					{
						if((dessin.get(premier_Y-1).get(premier_X-1)))//sens 5
						{
							sensPrecedent=5;
							result.add((short)5);
							suivant_Y=(short)(premier_Y-1);
							suivant_X=(short)(premier_X-1);
						}
					}
				}
			}
			
			while(suivant_Y!=premier_Y || suivant_X!=premier_X)
				//boucle qui gère le contour
				//refait 7
			{
				switch (sensPrecedent)
				{
				case 0:
				{
					if(dessin.get(suivant_Y+1).get(suivant_X-1))//sens 7
					{
						sensPrecedent=7;
						result.add((short)7);
						suivant_Y=(short)(suivant_Y+1);
						suivant_X=(short)(suivant_X-1);
					}
					else{
						if((dessin.get(suivant_Y+1).get(suivant_X)))//sens 0
						{
							sensPrecedent=0;
							result.add((short)0);
							suivant_Y=(short)(suivant_Y+1);
							suivant_X=(short)(suivant_X);
						}
						else
						{
							if((dessin.get(suivant_Y+1).get(suivant_X+1)))//sens 1
							{
								sensPrecedent=1;
								result.add((short)1);
								suivant_Y=(short)(suivant_Y+1);
								suivant_X=(short)(suivant_X+1);
							}
							else
							{
								if((dessin.get(suivant_Y).get(suivant_X+1)))//sens 2
								{
									sensPrecedent=2;
									result.add((short)2);
									suivant_Y=(short)(suivant_Y);
									suivant_X=(short)(suivant_X+1);
								}
								else
								{
									if((dessin.get(suivant_Y-1).get(suivant_X+1)))//sens 3
									{
										sensPrecedent=3;
										result.add((short)3);
										suivant_Y=(short)(suivant_Y-1);
										suivant_X=(short)(suivant_X+1);
									}
								}
							}
						}
					}
	
					break;
				}
				case 1:
				{
					if(dessin.get(suivant_Y+1).get(suivant_X-1))//sens 7
					{
						sensPrecedent=7;
						result.add((short)7);
						suivant_Y=(short)(suivant_Y+1);
						suivant_X=(short)(suivant_X-1);
					}
					else{
						if((dessin.get(suivant_Y+1).get(suivant_X)))//sens 0
						{
							sensPrecedent=0;
							result.add((short)0);
							suivant_Y=(short)(suivant_Y+1);
							suivant_X=(short)(suivant_X);
						}
						else
						{
							if((dessin.get(suivant_Y+1).get(suivant_X+1)))//sens 1
							{
								sensPrecedent=1;
								result.add((short)1);
								suivant_Y=(short)(suivant_Y+1);
								suivant_X=(short)(suivant_X+1);
							}
							else
							{
								if((dessin.get(suivant_Y).get(suivant_X+1)))//sens 2
								{
									sensPrecedent=2;
									result.add((short)2);
									suivant_Y=(short)(suivant_Y);
									suivant_X=(short)(suivant_X+1);
								}
								else
								{
									if((dessin.get(suivant_Y-1).get(suivant_X+1)))//sens 3
									{
										sensPrecedent=3;
										result.add((short)3);
										suivant_Y=(short)(suivant_Y-1);
										suivant_X=(short)(suivant_X+1);
									}
									else
									{
										if((dessin.get(suivant_Y-1).get(suivant_X)))//sens 4
										{
											sensPrecedent=4;
											result.add((short)4);
											suivant_Y=(short)(suivant_Y-1);
											suivant_X=(short)(suivant_X);
										}
									}
								}
							}
						}
					}
	
					break;
				}
				case 2:
				{
					if(dessin.get(suivant_Y+1).get(suivant_X+1))//sens 1
					{
						sensPrecedent=1;
						result.add((short)1);
						suivant_Y=(short)(suivant_Y+1);
						suivant_X=(short)(suivant_X+1);
					}
					else{
						if((dessin.get(suivant_Y).get(suivant_X+1)))//sens 2
						{
							sensPrecedent=2;
							result.add((short)2);
							suivant_Y=(short)(suivant_Y);
							suivant_X=(short)(suivant_X+1);
						}
						else
						{
							if((dessin.get(suivant_Y-1).get(suivant_X+1)))//sens 3
							{
								sensPrecedent=3;
								result.add((short)3);
								suivant_Y=(short)(suivant_Y-1);
								suivant_X=(short)(suivant_X+1);
							}
							else
							{
								if((dessin.get(suivant_Y-1).get(suivant_X)))//sens 4
								{
									sensPrecedent=4;
									result.add((short)4);
									suivant_Y=(short)(suivant_Y-1);
									suivant_X=(short)(suivant_X);
								}
								else
								{
									if((dessin.get(suivant_Y-1).get(suivant_X-1)))//sens 5
									{
										sensPrecedent=5;
										result.add((short)5);
										suivant_Y=(short)(suivant_Y-1);
										suivant_X=(short)(suivant_X-1);
									}
								}
							}
						}
					}
	
					break;
				}
				case 3:
				{
					if(dessin.get(suivant_Y+1).get(suivant_X+1))//sens 1
					{
						sensPrecedent=1;
						result.add((short)1);
						suivant_Y=(short)(suivant_Y+1);
						suivant_X=(short)(suivant_X+1);
					}
					else{
						if((dessin.get(suivant_Y).get(suivant_X+1)))//sens 2
						{
							sensPrecedent=2;
							result.add((short)2);
							suivant_Y=(short)(suivant_Y);
							suivant_X=(short)(suivant_X+1);
						}
						else
						{
							if((dessin.get(suivant_Y-1).get(suivant_X+1)))//sens 3
							{
								sensPrecedent=3;
								result.add((short)3);
								suivant_Y=(short)(suivant_Y-1);
								suivant_X=(short)(suivant_X+1);
							}
							else
							{
								if((dessin.get(suivant_Y-1).get(suivant_X)))//sens 4
								{
									sensPrecedent=4;
									result.add((short)4);
									suivant_Y=(short)(suivant_Y-1);
									suivant_X=(short)(suivant_X);
								}
								else
								{
									if((dessin.get(suivant_Y-1).get(suivant_X-1)))//sens 5
									{
										sensPrecedent=5;
										result.add((short)5);
										suivant_Y=(short)(suivant_Y-1);
										suivant_X=(short)(suivant_X-1);
									}
									else
									{
										if((dessin.get(suivant_Y).get(suivant_X-1)))//sens 6
										{
											sensPrecedent=6;
											result.add((short)6);
											suivant_Y=(short)(suivant_Y);
											suivant_X=(short)(suivant_X-1);
										}
									}
								}
							}
						}
					}
					break;
				}
				case 4:
				{
					if(dessin.get(suivant_Y-1).get(suivant_X+1))//sens 3
					{
						sensPrecedent=3;
						result.add((short)3);
						suivant_Y=(short)(suivant_Y-1);
						suivant_X=(short)(suivant_X+1);
					}
					else{
						if((dessin.get(suivant_Y-1).get(suivant_X)))//sens 4
						{
							sensPrecedent=4;
							result.add((short)4);
							suivant_Y=(short)(suivant_Y-1);
							suivant_X=(short)(suivant_X);
						}
						else
						{
							if((dessin.get(suivant_Y-1).get(suivant_X-1)))//sens 5
							{
								sensPrecedent=5;
								result.add((short)5);
								suivant_Y=(short)(suivant_Y-1);
								suivant_X=(short)(suivant_X-1);
							}
							else
							{
								if((dessin.get(suivant_Y).get(suivant_X-1)))//sens 6
								{
									sensPrecedent=6;
									result.add((short)6);
									suivant_Y=(short)(suivant_Y);
									suivant_X=(short)(suivant_X-1);
								}
								else
								{
									if((dessin.get(suivant_Y+1).get(suivant_X-1)))//sens 7
									{
										sensPrecedent=7;
										result.add((short)7);
										suivant_Y=(short)(suivant_Y+1);
										suivant_X=(short)(suivant_X-1);
									}
								}
							}
						}
					}
					break;
				}
				case 5:
				{
					if(dessin.get(suivant_Y-1).get(suivant_X+1))//sens 3
					{
						sensPrecedent=3;
						result.add((short)3);
						suivant_Y=(short)(suivant_Y-1);
						suivant_X=(short)(suivant_X+1);
					}
					else{
						if((dessin.get(suivant_Y-1).get(suivant_X)))//sens 4
						{
							sensPrecedent=4;
							result.add((short)4);
							suivant_Y=(short)(suivant_Y-1);
							suivant_X=(short)(suivant_X);
						}
						else
						{
							if((dessin.get(suivant_Y-1).get(suivant_X-1)))//sens 5
							{
								sensPrecedent=5;
								result.add((short)5);
								suivant_Y=(short)(suivant_Y-1);
								suivant_X=(short)(suivant_X-1);
							}
							else
							{
								if((dessin.get(suivant_Y).get(suivant_X-1)))//sens 6
								{
									sensPrecedent=6;
									result.add((short)6);
									suivant_Y=(short)(suivant_Y);
									suivant_X=(short)(suivant_X-1);
								}
								else
								{
									if((dessin.get(suivant_Y+1).get(suivant_X-1)))//sens 7
									{
										sensPrecedent=7;
										result.add((short)7);
										suivant_Y=(short)(suivant_Y+1);
										suivant_X=(short)(suivant_X-1);
									}
									else
									{
										if((dessin.get(suivant_Y+1).get(suivant_X)))//sens 0
										{
											sensPrecedent=0;
											result.add((short)0);
											suivant_Y=(short)(suivant_Y+1);
											suivant_X=(short)(suivant_X);
										}
									}
								}
							}
						}
					}
					break;
				}
				case 6:
				{
					if(dessin.get(suivant_Y-1).get(suivant_X-1))//sens 5
					{
						sensPrecedent=5;
						result.add((short)5);
						suivant_Y=(short)(suivant_Y-1);
						suivant_X=(short)(suivant_X-1);
					}
					else{
						if((dessin.get(suivant_Y).get(suivant_X-1)))//sens 6
						{
							sensPrecedent=6;
							result.add((short)6);
							suivant_Y=(short)(suivant_Y);
							suivant_X=(short)(suivant_X-1);
						}
						else
						{
							if((dessin.get(suivant_Y+1).get(suivant_X-1)))//sens 7
							{
								sensPrecedent=7;
								result.add((short)7);
								suivant_Y=(short)(suivant_Y+1);
								suivant_X=(short)(suivant_X-1);
							}
							else
							{
								if((dessin.get(suivant_Y+1).get(suivant_X)))//sens 0
								{
									sensPrecedent=0;
									result.add((short)0);
									suivant_Y=(short)(suivant_Y+1);
									suivant_X=(short)(suivant_X);
								}
								else
								{
									if((dessin.get(suivant_Y+1).get(suivant_X+1)))//sens 1
									{
										sensPrecedent=1;
										result.add((short)1);
										suivant_Y=(short)(suivant_Y+1);
										suivant_X=(short)(suivant_X+1);
									}
								}
							}
						}
					}
					break;
				}
				case 7:
				{
					if(dessin.get(suivant_Y-1).get(suivant_X-1))//sens 5
					{
						sensPrecedent=5;
						result.add((short)5);
						suivant_Y=(short)(suivant_Y-1);
						suivant_X=(short)(suivant_X-1);
					}
					else{
						if((dessin.get(suivant_Y).get(suivant_X-1)))//sens 6
						{
							sensPrecedent=6;
							result.add((short)6);
							suivant_Y=(short)(suivant_Y);
							suivant_X=(short)(suivant_X-1);
						}
						else
						{
							if((dessin.get(suivant_Y+1).get(suivant_X-1)))//sens 7
							{
								sensPrecedent=7;
								result.add((short)7);
								suivant_Y=(short)(suivant_Y+1);
								suivant_X=(short)(suivant_X-1);
							}
							else
							{
								if((dessin.get(suivant_Y+1).get(suivant_X)))//sens 0
								{
									sensPrecedent=0;
									result.add((short)0);
									suivant_Y=(short)(suivant_Y+1);
									suivant_X=(short)(suivant_X);
								}
								else
								{
									if((dessin.get(suivant_Y+1).get(suivant_X+1)))//sens 1
									{
										sensPrecedent=1;
										result.add((short)1);
										suivant_Y=(short)(suivant_Y+1);
										suivant_X=(short)(suivant_X+1);
									}
									else
									{
										if((dessin.get(suivant_Y).get(suivant_X+1)))//sens 2
										{
											sensPrecedent=2;
											result.add((short)2);
											suivant_Y=(short)(suivant_Y);
											suivant_X=(short)(suivant_X+1);
										}
									}
								}
							}
						}
					}
					break;
				}
				default :
				{
					break;
				}
				}
			}
		}
	}

	public void afficheResult(){
		for(int i=0;i<result.size();i++)
			System.out.print(result.get(i));
		if(result.size()==0)
			System.out.print(RdC.elementsTraduction[37]);
		System.out.println();
	}

	public Vector<Short> getResult() {
		return result;
	}

	public void setResult(Vector<Short> result) {
		this.result = result;
	}



}
