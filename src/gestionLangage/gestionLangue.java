package gestionLangage;

import java.io.File;
import java.util.Vector;

public class gestionLangue{

	private String[] languesDispo;
	
	public gestionLangue(String s){
		Vector<String> v = listeXML(s);
		
		if (v == null)
			this.languesDispo = null;
		else {
			this.languesDispo = new String[v.size()];
			int i=0;
			while (i<languesDispo.length){
				this.languesDispo[i] = (String) v.elementAt(i);
				i+=1;
			}
		}
		
	}
	
	public String[] getLanguesDisponibles(){
		return this.languesDispo;
	}
	
	public Vector<String> listeXML(String repertoire){		
		Vector<String> retour = null;	
		
		try{			
			File f = new File(repertoire);
			if (!f.exists() || !f.isDirectory()){
				System.err.println(repertoire+"n'est pas un repertoire !");
				return null;
			}
			String nomComplet = repertoire.endsWith(File.separator) ? repertoire  : repertoire + File.separator;
			
			/*equivalent a if repertoire.endsWith(File.separator)
			 *                   nomComplet = repertoire;
			 *             else nomComplet = repertoire + File.separator;
			 */
			//System.err.println(f.getAbsolutePath());
			String[] liste = f.list();
			retour = new Vector<String>();	
			for (int i=0;i<liste.length;i++){
				File fs = new File(nomComplet + liste[i]);				
				if (!fs.isDirectory()){					
					if (fs.getName().endsWith(".xml")){				
						retour.add(fs.getName().substring(0, fs.getName().length() - 4));
					}
				}
			}			
		}
		catch (Exception e){
			System.err.println(e);
		}
		return retour;
	}
}
