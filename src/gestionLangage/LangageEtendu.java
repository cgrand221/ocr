package gestionLangage;

import java.io.FileInputStream;
import java.io.InputStream;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.xml.sax.helpers.DefaultHandler;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class LangageEtendu extends DefaultHandler{

	String elementTraduction[];
	public String nomDico;
	public LangageEtendu(String fichier){
		//System.out.println(fichier);
		this.elementTraduction = new String[145];
		try{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			
			InputStream is = new FileInputStream(fichier);
			Document document = builder.parse(is);
			NodeList l = document.getDocumentElement().getElementsByTagName("ElementTraduction");
			int i = 0;
			while (i < this.elementTraduction.length){
				Element e =  (Element) l.item(i);
				String s = e.getAttribute("phrase");
				this.elementTraduction[i] = s;
				i++;
			}
			l = document.getDocumentElement().getElementsByTagName("dictionnaire");
			Element e =  (Element) l.item(0);
			this.nomDico = e.getAttribute("nom");
		}
		catch (Exception e){
			System.err.println("aa"+e);
		}
	}
	public String[] getElementTraduction(){
		return this.elementTraduction;
	}
	public String getNomDico(){
		return this.nomDico;
	}
}
