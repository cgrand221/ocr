/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * BaseInterface.java
 *
 * Created on 4 févr. 2010, 09:54:29
 */

package baseApp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import rdc.*;
import distanceEdition.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;




/**
 *
 * @author gui
 */
public class BaseInterface extends javax.swing.JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6579833702216148485L;
	public BaseApprentissage ba;
	Vector<String> columnNames;
	Vector<Vector<Element>> data;
	static String etiquette;
	String current;
	ImageIcon reussite = new ImageIcon("./images/login.png");
	ImageIcon echec = new ImageIcon("./images/stop.png");
	boolean est_reduit;

	/** Creates new form BaseInterface 
	 * @param  
	 * @param panneau */
	public BaseInterface() {
		est_reduit=false;
		JPanel d = new DistanceEdition();
		//this.add(d);
		this.ba = new BaseApprentissage((DistanceEdition) d);
		this.ba.lecture(this.ba.url);
		current = ba.url;
		System.out.println(RdC.elementsTraduction[100]+current+RdC.elementsTraduction[101]+ba.getBase().size()+RdC.elementsTraduction[102]);

		//this.panneauBase = panneau;

		initable();
		initComponents();
		setTable();


		fich.setText(current);

		//this.table = new JTable(data, columnNames);
		//this.table.repaint();
		//this.table.add("coucou", table.getComponentAt(1, 1));
		//this.repaint();

		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				if(e.getButton()==MouseEvent.BUTTON3){
					//System.out.println(e.getButton());
					JFrame frame = new JFrame();

					if(table.getSelectedColumnCount()!=0){
						Element element = data.elementAt(table.getSelectedRow()).elementAt(table.getSelectedColumn());

						if(element.etiquette!=' '){
							ImageIcon image = new ImageIcon(element.getChemin());
							//System.out.println(element.getChemin());
							JLabel label = new JLabel(image);
							label.setToolTipText(element.getChemin());

							String valeur = new String();
							/*char tmp[] = element.getContour().toString().toCharArray();
        				for(int i=0;i<tmp.length;i++){
        				valeur+=tmp[i];
        				if(i%40==0)
        					valeur+="\n";
        				}*/
							valeur = element.getContour().toString();
							//System.out.println(valeur);
							JTextField area = new JTextField(15);
							area.setText(valeur);
							area.setEditable(false);

							//label.add(area);
							frame.setTitle(RdC.elementsTraduction[103]+element.getEtiquette());

							frame.setLayout(new BorderLayout());
							frame.add(area,BorderLayout.NORTH);
							frame.add(label,BorderLayout.SOUTH);  

							Toolkit k = Toolkit.getDefaultToolkit();
							Dimension tailleEcran = k.getScreenSize();
							int longeurEcran = tailleEcran.width;
							int hauteurEcran = tailleEcran.height;

							frame.setLocation(longeurEcran/2-ImageObserver.WIDTH/2, hauteurEcran/2-ImageObserver.HEIGHT/2);

							frame.setResizable(true);
							frame.pack();
							frame.setVisible(true);
						}
					}
				}
			}
		});

		etiquetteField.addKeyListener(new KeyListener() {    	
			public void keyPressed(KeyEvent e) {
				//System.out.println(e.getKeyCode());
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					ajout();       		    	
				if (e.getKeyCode() == KeyEvent.VK_DELETE)
					suppr();
			}
			@Override
			public void keyReleased(KeyEvent e) {
			}
			@Override
			public void keyTyped(KeyEvent e) {	
			}
		});


		table.addKeyListener(new KeyListener() {    	
			public void keyPressed(KeyEvent e) {
				//System.out.println(e.getKeyCode());
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					ajout();       		    	
				if (e.getKeyCode() == KeyEvent.VK_DELETE)
					suppr();
			}
			@Override
			public void keyReleased(KeyEvent e) {
			}
			@Override
			public void keyTyped(KeyEvent e) {	
			}
		});



		/*
        this.addKeyListener(new KeyListener() {

        	public void keyPressed(KeyEvent e) {

        		System.out.println(e.getKeyCode());
        		if (e.getKeyCode() == KeyEvent.VK_ENTER)
        			ajout();       		    	
        		if (e.getKeyCode() == KeyEvent.VK_DELETE)
        			suppr();
    			}

			@Override
			public void keyReleased(KeyEvent e) {
			}
			@Override
			public void keyTyped(KeyEvent e) {	
			}
        });
		 */
	}

	private void initComponents() {

		spane = new javax.swing.JScrollPane();
		table = new javax.swing.JTable();
		suppr = new javax.swing.JButton();
		supprAll = new javax.swing.JButton();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		load = new javax.swing.JButton();
		save = new javax.swing.JButton();
		saveAs = new javax.swing.JButton();
		fich = new javax.swing.JLabel();
		etiquetteField = new javax.swing.JTextField();
		boutonAjouter = new javax.swing.JButton();
		panneauBase = new rdc.Panneau();
		//System.out.println("1");
		effacer = new javax.swing.JButton();
		reduire = new javax.swing.JButton();
		condenser = new javax.swing.JButton();

		table.setModel((new javax.swing.table.DefaultTableModel(data,columnNames)));
		table.setColumnSelectionAllowed(true);
		spane.setViewportView(table);
		//spane.add(table);
		//spane.setHorizontalScrollBarPolicy(spane.HORIZONTAL_SCROLLBAR_ALWAYS);
		//table.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

		setPreferredSize(new java.awt.Dimension(1000, 420));

		suppr.setText(RdC.elementsTraduction[104]);
		suppr.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprActionPerformed(evt);
			}
		});

		supprAll.setText(RdC.elementsTraduction[105]);
		supprAll.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				supprAllActionPerformed(evt);
			}
		});

		jLabel1.setFont(new java.awt.Font("Arial", 1, 18));
		jLabel1.setText(RdC.elementsTraduction[106]);
		jLabel2.setText(RdC.elementsTraduction[107]);

		load.setText(RdC.elementsTraduction[108]);
		load.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				loadActionPerformed(evt);
			}
		});

		save.setText(RdC.elementsTraduction[87]);
		save.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				saveActionPerformed(evt);
			}
		});

		saveAs.setText(RdC.elementsTraduction[109]);
		saveAs.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				saveAsActionPerformed(evt);
			}
		});

		boutonAjouter.setText(RdC.elementsTraduction[110]);
		boutonAjouter.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				boutonAjouterActionPerformed(evt);
			}
		});

		effacer.setText(RdC.elementsTraduction[111]);
		effacer.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				effacerActionPerformed(evt);
			}
		});

		reduire = new JButton(RdC.elementsTraduction[112]);
		reduire.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				reduireActionPerformed(evt);
			}
		});

		condenser = new JButton(RdC.elementsTraduction[113]);
		condenser.addActionListener(new java.awt.event.ActionListener()
		{
			public void actionPerformed(java.awt.event.ActionEvent evt)
			{
				condenserActionPerformed(evt);
			}
		});

		this.setBackground(Color.lightGray);

		//panneauBase.setMinimumSize(new java.awt.Dimension(123, 153));
		//panneauBase.setPreferredSize(new java.awt.Dimension(123, 153));

		javax.swing.GroupLayout panneauBaseLayout = new javax.swing.GroupLayout(panneauBase);
		panneauBase.setLayout(panneauBaseLayout);
		panneauBaseLayout.setHorizontalGroup(
				panneauBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 120, Short.MAX_VALUE)
		);
		panneauBaseLayout.setVerticalGroup(
				panneauBaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGap(0, 150, Short.MAX_VALUE)
		);


		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
								.addGroup(layout.createSequentialGroup()
										.addComponent(jLabel1)
										.addGap(297, 297, 297)
										.addComponent(fich, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
										.addGap(71, 71, 71))
										.addComponent(spane, javax.swing.GroupLayout.PREFERRED_SIZE, 755, javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
												.addGroup(layout.createSequentialGroup()
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
																		.addComponent(effacer)
																		.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 88, Short.MAX_VALUE)
																		.addComponent(boutonAjouter))
																		.addComponent(suppr, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
																		.addComponent(supprAll, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
																		.addComponent(load, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
																		.addComponent(save, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
																		.addComponent(saveAs, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
																		.addComponent(reduire, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
																		.addComponent(condenser, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)))
																		.addGroup(layout.createSequentialGroup()
																				.addGap(49, 49, 49)
																				.addComponent(panneauBase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
																				.addGroup(layout.createSequentialGroup()
																						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																						.addComponent(jLabel2)
																						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 98, Short.MAX_VALUE)
																						.addComponent(etiquetteField, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
																						.addContainerGap())
		);
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
						.addContainerGap()
						.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
								.addComponent(jLabel1)
								.addComponent(suppr)
								.addComponent(fich, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE))
								.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
										.addGroup(layout.createSequentialGroup()
												.addGap(12, 12, 12)
												.addComponent(spane, javax.swing.GroupLayout.DEFAULT_SIZE, 437, Short.MAX_VALUE))
												.addGroup(layout.createSequentialGroup()
														.addGap(6, 6, 6)
														.addComponent(supprAll)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(load)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(save)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(saveAs)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(reduire)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(condenser)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addComponent(panneauBase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
														.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
														.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
																.addComponent(jLabel2)
																.addComponent(etiquetteField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
																.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
																.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
																		.addComponent(effacer)
																		.addComponent(boutonAjouter))))
																		.addContainerGap())
		);


		//spane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		//this.add(spane);
		//this.add(table);

	}// </editor-fold>

	private void loadActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		String s = new String();
		JFileChooser jfc = new JFileChooser();
		jfc.setCurrentDirectory(new File("."));
		jfc.setFileFilter(new FiltreFichier());
		jfc.setAcceptAllFileFilterUsed(false);
		if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
			s = jfc.getSelectedFile().toString();
			String tmp[] = s.split("/");
			current = tmp[tmp.length-1];
			data.clear();
			columnNames.clear();
			this.ba.base.clear();
			this.ba.distancesDejaCalculees = new Vector<Vector<Integer>>();
			this.ba.lecture(s);
			initable();
			setTable();
			//table.setModel((new javax.swing.table.DefaultTableModel(data,columnNames)));
			if(current.length()>35)
				current = "..."+current.substring(current.length()-35, current.length());
			System.out.println(RdC.elementsTraduction[100]+current+RdC.elementsTraduction[101]+ba.getBase().size()+RdC.elementsTraduction[102]);
			fich.setText(current);
			this.repaint();
		}
	}

	private void saveActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		this.ba.ecriture(current);
		System.out.println(RdC.elementsTraduction[100]+current+RdC.elementsTraduction[114]+ba.getBase().size()+RdC.elementsTraduction[102]);
	}

	private void supprAllActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		data.clear();
		columnNames.clear();
		ba.base.clear();
		this.ba.distancesDejaCalculees = new Vector<Vector<Integer>>();
		initable();
		//table.setModel((new javax.swing.table.DefaultTableModel(data,columnNames)));
		setTable();
	}


	private void supprActionPerformed(java.awt.event.ActionEvent evt) {
		suppr();
	}

	private void suppr() {
		// TODO add your handling code here:
		int[] lig = table.getSelectedRows();
		int[] col = table.getSelectedColumns();

		//System.out.println(lig+" "+col);
		for(int i=0;i<lig.length;i++){
			for(int j=0;j<col.length;j++){
				Element el = data.elementAt(lig[i]).elementAt(col[j]);
				if(el.etiquette!=' '){
					int indice = ba.base.indexOf(el);
					ba.base.removeElement(el);
					Reduction re = new Reduction(ba,ba.de);
					re.suppression(indice, ba.distancesDejaCalculees);
				}
			}
		}
		initable();
		setTable();
		//table.setModel((new javax.swing.table.DefaultTableModel(data,columnNames)));
	}

	private void saveAsActionPerformed(java.awt.event.ActionEvent evt) {
		JFileChooser jfc = new JFileChooser();
		jfc.setFileFilter(new FiltreFichier());
		jfc.setAcceptAllFileFilterUsed(false);
		jfc.setCurrentDirectory(new File("."));
		if (jfc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
			String s = jfc.getSelectedFile().toString();
			String tmp[] = s.split("/");
			current = tmp[tmp.length-1];
			ba.ecriture(s);
			if(current.length()>35)
				current = "..."+current.substring(current.length()-35, current.length());
			System.out.println(RdC.elementsTraduction[100]+current+RdC.elementsTraduction[114]+ba.getBase().size()+RdC.elementsTraduction[102]);
			fich.setText(current);
		}
	}

	private void boutonAjouterActionPerformed(java.awt.event.ActionEvent evt) {
		ajout();
	}

	private void ajout(){
		etiquette = etiquetteField.getText().trim();

		if(!etiquette.isEmpty()){
			Contour contour = new Contour(panneauBase.getMatrice());
			Vector<Short> resultat = contour.getResult();
			if(!resultat.isEmpty())
			{

				//rajout ***
				BufferedImage img1 = new BufferedImage(panneauBase.getWidth(), panneauBase.getHeight(), BufferedImage.TYPE_INT_RGB);
				Graphics2D g2 = img1.createGraphics();
				panneauBase.paint(g2);
				String nomImage = new String();
				try
				{
					String str = "";
					for(int i=0; i<resultat.size(); i++)
						str += resultat.elementAt(i);
					byte[] bytes = str.getBytes();
					java.util.zip.CRC32 x = new java.util.zip.CRC32();
					x.update(bytes);
					//System.out.println("CRC32 = " + Long.toHexString(x.getValue()));
					String extension = "png";
					nomImage = "./Alphabet/"+etiquette.charAt(0)+"_"+Long.toHexString(x.getValue())+"."+extension;                  
					new File("./Alphabet").mkdir();
					ImageIO.write(img1, extension, new File(nomImage));
					System.out.println(RdC.elementsTraduction[115] + nomImage);
				}
				catch (FileNotFoundException fnfe) {
					System.out.println(RdC.elementsTraduction[95]);	
				} catch (IOException e) {
					System.out.println(RdC.elementsTraduction[64]);
				}

				Element element = new Element();
				element.setEtiquette(etiquette.charAt(0));
				element.setContour(resultat);
				element.setChemin(nomImage);
				System.out.println(element.chemin+"!");
				this.ba.ajouter(element, true);
				initable();
				setTable();

				JOptionPane.showMessageDialog(null, RdC.elementsTraduction[116]+etiquette.charAt(0), RdC.elementsTraduction[117],
						JOptionPane.INFORMATION_MESSAGE,reussite);
				panneauBase.efface();
				etiquetteField.setText(null);
			}
		}
		else
		{
			JOptionPane.showMessageDialog(null, RdC.elementsTraduction[118], RdC.elementsTraduction[52],
					JOptionPane.ERROR_MESSAGE,echec);
		}
	}


	private void effacerActionPerformed(java.awt.event.ActionEvent evt) {
		panneauBase.efface();
		etiquetteField.setText(null);
	}

	private void reduireActionPerformed(java.awt.event.ActionEvent evt) {
		est_reduit=true;
		int n1 = ba.getBase().size();
		if (n1 == 0)
		{
			ImageIcon echec = new ImageIcon("./images/stop.png");
			JOptionPane.showMessageDialog(
					null,
					RdC.elementsTraduction[119],
					RdC.elementsTraduction[120], JOptionPane.ERROR_MESSAGE, echec);
		}
		else
		{
			Reduction r = new Reduction(this.ba,this.ba.de);
			ba.setBase(r.reduire());
			initable();
			setTable();
			int n2 = ba.getBase().size();
			String masque = new String("#0");
			DecimalFormat df = new DecimalFormat(masque);
			String resultat = df.format(((float) (n1-n2))/n1*100);
			JOptionPane.showMessageDialog(
					null,
					RdC.elementsTraduction[121]+resultat+"% ("+RdC.elementsTraduction[122]+n1+RdC.elementsTraduction[123]+n2+RdC.elementsTraduction[124],
					RdC.elementsTraduction[125],
					JOptionPane.INFORMATION_MESSAGE, reussite);
		}
	}

	public static void wait_temp(int n)
	{

		long t0, t1;

		t0 =  System.currentTimeMillis();

		do{
			t1 = System.currentTimeMillis();
		}
		while ((t1 - t0) < (n * 1000));
	}


	private void condenserActionPerformed(java.awt.event.ActionEvent evt)
	{
		Condensation cond=new Condensation(this.ba, this.ba.de);

		/*
		ici ***
		JFrame f = new JFrame();
		Dialogue dialogue = new Dialogue(f,"Condensation", false);
		dialogue.setSize(120, 400);
		dialogue.pack();
		Toolkit k = Toolkit.getDefaultToolkit();
		Dimension tailleEcran = k.getScreenSize();
		int longeurEcran = tailleEcran.width;
		int hauteurEcran = tailleEcran.height;
		dialogue.setLocation(longeurEcran/2-400/2, hauteurEcran/2-120/2);
		dialogue.setAlwaysOnTop(true);
		dialogue.setResizable(false);
*/
		if(!est_reduit)
		{           
			int reponse = JOptionPane.showConfirmDialog(null, RdC.elementsTraduction[126], RdC.elementsTraduction[127], JOptionPane.YES_NO_OPTION);
			if (reponse == JOptionPane.YES_OPTION)
			{
				//dialogue.setVisible(true);
				//dialogue.repaint();
				System.out.println(RdC.elementsTraduction[128]);
				this.ba=cond.condenser();
				est_reduit=false;
				initable();
				setTable();
				//dialogue.setVisible(false);
				System.out.println(RdC.elementsTraduction[129]);
			}
			else 
				;
		}
		else
		{
			System.out.println(RdC.elementsTraduction[130]);
			//dialogue.setVisible(true);
			this.ba=cond.condenser();
			est_reduit=false;
			initable();
			setTable();
			//dialogue.setVisible(false);
			System.out.println(RdC.elementsTraduction[129]);
		}
	}



	// Variables declaration - do not modify
	private javax.swing.JButton boutonAjouter;
	private javax.swing.JButton effacer;
	private javax.swing.JTextField etiquetteField;
	private javax.swing.JLabel fich;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JButton load;
	private javax.swing.JButton save;
	private javax.swing.JButton saveAs;
	private javax.swing.JScrollPane spane;
	private javax.swing.JButton suppr;
	private javax.swing.JButton supprAll;
	private javax.swing.JTable table;
	public static rdc.Panneau panneauBase;//*** modifie ! ***
	public JButton reduire;
	public JButton condenser;
	// End of variables declaration


	public void initable(){
		int n = ba.base.size();
		int ncol=0;
		int nlig=0;

		columnNames = new Vector<String>();
		data = new Vector<Vector<Element>>();
		for(int i=0;i<n;i++){
			String etiquette = String.valueOf(ba.base.elementAt(i).getEtiquette());
			if(!columnNames.contains(etiquette)){
				columnNames.add(etiquette);
				ncol++;
			}
		}

		Collections.sort(columnNames);

		data.add(new Vector<Element>());
		for(int k=0;k<=ncol;k++){
			data.elementAt(0).add(new Element());
		}

		int newligne = 1;

		for(int i=0;i<n;i++){
			String etiquette = String.valueOf(ba.base.elementAt(i).getEtiquette());
			int col = columnNames.indexOf(etiquette);
			//System.out.println(ncol+" "+col);
			int lig = 0;
			//System.out.println(data);
			while(data.elementAt(lig).elementAt(col).getEtiquette()!=' '){
				lig++;
				if(lig>nlig){
					nlig++;
					newligne = 1;
				}
			}
			if(newligne==1){
				data.add(new Vector<Element>());
				for(int k=0;k<=ncol;k++){
					data.elementAt(lig+1).add(new Element());
				}
				newligne=0;
			}
			data.elementAt(lig).setElementAt(ba.base.elementAt(i), col);
		}
		data.removeElementAt(data.size()-1);
	}


	public void setTable(){

		//table=new JTable();
		table.setModel((new MyTableModel(columnNames,data)));
		//table=new JTable(data,columnNames);
		table.setColumnSelectionAllowed(true);
		table.setDefaultRenderer(Object.class, new ImageRenderer());

		table.setRowHeight(55);
		//table.setFillsViewportHeight(true);
		//table.setPreferredScrollableViewportSize(new Dimension(600, 600));
		//table.setSize(800, 400);

		TableColumn column = null;
		for (int i = 0; i < columnNames.size(); i++) {
			column = table.getColumnModel().getColumn(i);
			column.setMaxWidth(52);
			column.setMinWidth(52);
		}

		//JPanel panel = new JPanel();
		//spane = new JScrollPane(table);
		spane.setViewportView(table);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		// panel.add(spane);
		//this.add(spane);



	}

	class MyTableModel extends AbstractTableModel {
		private static final long serialVersionUID = 1L;

		Vector<String> columnNames;
		Vector<Vector<Element>> data;

		public MyTableModel(Vector<String> columnNames,Vector<Vector<Element>> data){
			this.columnNames = columnNames;
			this.data = data;
		}

		public int getColumnCount() {
			return columnNames.size();
		}

		public int getRowCount() {
			return data.size();
		}

		public String getColumnName(int col) {
			return columnNames.elementAt(col);
		}

		public Object getValueAt(int row, int col) {
			return data.elementAt(row).elementAt(col);
		}

		@SuppressWarnings("unchecked")
		public Class getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}

		public boolean isCellEditable(int row, int col) {
			return false;
		}

	}

}
