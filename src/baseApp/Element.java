package baseApp;

import java.util.Vector;

public class Element implements Comparable<Element>{

	char etiquette;
	Vector<Short> contour;
    String chemin;
	
	public Element(){
        this.etiquette = ' ';
		this.contour = new Vector<Short>(500);
	}
	
	public Element(char etiquette, Vector<Short> contour){
		this.etiquette = etiquette;
		this.contour = contour;
	}

    
	public Element(char etiquette, Vector<Short> contour, String chemin){
		this.etiquette = etiquette;
		this.contour = contour;
		this.chemin = chemin;
	}
	

	public char getEtiquette() {
		return etiquette;
	}

	public void setEtiquette(char etiquette) {
		this.etiquette = etiquette;
	}

	public Vector<Short> getContour() {
		return contour;
	}

	public void setContour(Vector<Short> contour) {
		this.contour = contour;
	}

    public String getChemin() {
        return chemin;
    }

    public void setChemin(String chemin) {
        this.chemin = chemin;
    }
    
    public void ajout(String cont){
    	
    	for (int i = 0 ; i < cont.length() ;i++)
    		this.contour.add(Short.valueOf(cont.substring(i, i+1)));
    }


    /*
	public Vector<Vector<Boolean>> getMatrice() {
		return matrice;
	}

	public void setMatrice(Vector<Vector<Boolean>> matrice) {
		this.matrice = matrice;
	}
    */

    @Override
    public int compareTo(Element element) {
		return -(this.etiquette - element.etiquette);
	}
    

	@Override
	public String toString() {
        if(etiquette==' ')
            return(" ");
        return(etiquette+" "+contour.toString());

	}
	
}
