package baseApp;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import rdc.RdC;


import distanceEdition.DistanceEdition;

public class ReductionInterface extends JPanel implements ActionListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton boutonReduction;
	private DistanceEdition de;
	private BaseApprentissage ba;
	//a mettre dans l'interface de la base d'apprentissage
	public ReductionInterface(DistanceEdition de,BaseApprentissage ba) {
		super();
		this.setPreferredSize(new Dimension(50,100));
		this.setLayout(new GridLayout(1,1));
		this.boutonReduction = new JButton(RdC.elementsTraduction[112]);
		this.boutonReduction.addActionListener(this);
		this.add(this.boutonReduction);
		this.de = de;
		this.ba = ba;
		this.setVisible(true);
	}

	
	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() == this.boutonReduction){
			Reduction r = new Reduction(this.ba,this.de);
			ba.setBase(r.reduire());
			ba.afficheDistancesDejaCalculees();
			
		}
		
	}
}
