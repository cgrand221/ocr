package baseApp;


import java.util.Hashtable;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;

import distanceEdition.DistanceEdition;


public class Reduction {
	private BaseApprentissage ba;
	
	Vector<Element> S;
	Vector<Short> SCleaned;
	private DistanceEdition de;
	public Reduction(BaseApprentissage ba,DistanceEdition de) {
		this.S = ba.getBase();
		this.ba = ba;
		this.de = de;
	}
	public Vector<Element> reduire(){
		Vector<Element> S1,S2;
		TreeSet<Integer> elementsSupprimes = new TreeSet<Integer>();
		S1 = new Vector<Element>();
		S2 = new Vector<Element>();
		Vector<Integer> ordre1 = new Vector<Integer>();
		Vector<Integer> ordre2 = new Vector<Integer>();
		Hashtable<Character,Integer> frequenceEtiquette = new Hashtable<Character, Integer>();
		splitIntoTwoSubSets(S1,S2,ordre1,ordre2,frequenceEtiquette);
		boolean stabilization = false;
		
		ProchesVoisinsRestreints pv = new ProchesVoisinsRestreints(null,ba,de,1);//pas de calcul de distances pour pv car on cherche a etiqueter des elements deja dans la base
		while (!stabilization){
			boolean stabilization1 = pv.classify(S1,S2,ordre1,ordre2,elementsSupprimes,frequenceEtiquette);
			boolean stabilization2 = pv.classify(S2,S1,ordre2,ordre1,elementsSupprimes,frequenceEtiquette);
			stabilization = stabilization1 && stabilization2;
		}
		union(S1,S2,ordre1,ordre2,elementsSupprimes);
		return this.S;
		//apres la reduction ba.distancesDejaCalculees est  toujours correcte !
	}
	private void union(Vector<Element> S1, Vector<Element> S2,Vector<Integer> ordre1,Vector<Integer> ordre2,TreeSet<Integer> elementsSupprimes) {
		//fait l'union des deux ensembles S1 et S2 en retablissant l'ordre de depart, sur les elements de S1 et de S2 qui existent toujours
		//supprime les distances deja calculees qui ne servent plus a rien
		this.S.removeAllElements();
		Iterator<Integer> it = elementsSupprimes.iterator();
		int i,j;
		i = 0;
		while (it.hasNext()){
			j = it.next();
			suppression(j-i,ba.distancesDejaCalculees);
			i++;
		}
		i = 0;
		j = 0;
		while ((i < S1.size()) && (j < S2.size())){//union en conservant l'ordre de depart
			if (ordre1.elementAt(i) < ordre2.elementAt(j)){
				S.add(S1.elementAt(i));
				i++;
			}
			else{//jamais d'egalite entre tous les elements de ordre1 et de ordre2
				S.add(S2.elementAt(j));
				j++;
			}
		}
		while (i < S1.size()){		
			S.add(S1.elementAt(i));
			i++;			
		}
		while (j < S2.size()){			
			S.add(S2.elementAt(j));
			j++;			
		}//fin union
	}
	
	public void suppression(int i,Vector<Vector<Integer>> distancesDejaCalculees) {
		//supprime la ieme distance dans distancesDejaCalculees
		for (int n  = 0;n < distancesDejaCalculees.size();n++){
			distancesDejaCalculees.elementAt(n).removeElementAt(i);
		}
		distancesDejaCalculees.removeElementAt(i);
	}
	
	public void splitIntoTwoSubSets(Vector<Element> S1,Vector<Element> S2,Vector<Integer> ordre1,Vector<Integer> ordre2,Hashtable<Character,Integer> frequenceEtiquette){
		//partage S en S1 et S2 en memorisant l'ordre de depart de S dans ordre1 et ordre2
		//ordre1.size() = S1.size()
		//ordre2.size() = S2.size()
		int alea;
		for (int i = 0;i<S.size();i++){
			alea = (int) (Math.random()*2);
			if (frequenceEtiquette.containsKey(S.elementAt(i).getEtiquette())){
				frequenceEtiquette.put(S.elementAt(i).getEtiquette(), frequenceEtiquette.get(S.elementAt(i).getEtiquette())+1);
			}
			else frequenceEtiquette.put(S.elementAt(i).getEtiquette(), 0);
			if (alea == 0){
				S1.add(S.elementAt(i));
				ordre1.add(i);
			}
			else{
				S2.add(S.elementAt(i));
				ordre2.add(i);
			}
		}
		/*
		Enumeration<Character> cles = frequenceEtiquette.keys();
		
		while (cles.hasMoreElements()){
			Character cle = cles.nextElement();
			System.err.println("etiquette = "+cle+", freq = "+frequenceEtiquette.get(cle));
		}*/
	}
}
