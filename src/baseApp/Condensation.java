package baseApp;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.text.DecimalFormat;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import rdc.Dialogue;
import rdc.RdC;

import voisins.ProchesVoisins;

import distanceEdition.DistanceEdition;

public class Condensation {

	BaseApprentissage ba;
	DistanceEdition de;
	ProchesVoisins pv;
	int taille_deb, taille_fin;

	public Condensation(BaseApprentissage ba, DistanceEdition de) {
		this.ba = ba;
		this.pv = new ProchesVoisins(null, ba, de);
		this.de = de;
	}

	public void affiche_element_base(BaseApprentissage ba) {
		for (int i = 0; i < ba.getBase().size(); i++) {
			System.out.println("e" + i + ": "	+ ba.getBase().elementAt(i).getEtiquette());
		}
	}

	public void affiche_tab(Vector<Integer> vi) {
		String s = "Tab int:\n<" + vi.elementAt(0) + ";";
		for (int i = 1; i < vi.size(); i++) {
			s += "" + vi.elementAt(i) + ";";
		}
		s += ">";
		//System.out.println(s);
	}

	public BaseApprentissage condenser() {
		BaseApprentissage STORAGE = new BaseApprentissage(this.de);
		Vector<Integer> poubelle = new Vector<Integer>();
		boolean Stabiliser = false;
		if (this.ba.getBase().size() == 0)
		{
			ImageIcon echec = new ImageIcon("./images/stop.png");
			JOptionPane
					.showMessageDialog(
							null,
							RdC.elementsTraduction[131],
							RdC.elementsTraduction[132], JOptionPane.ERROR_MESSAGE, echec);
			return this.ba;
		}
		Element es = this.ba.getBase().elementAt(0);
		STORAGE.ajouter(es, true);
		this.pv = new ProchesVoisins(null, null, de);
		//this.affiche_element_base(this.ba);
		this.taille_deb = this.ba.getBase().size();
		
		JFrame f = new JFrame();/* ici *** */
		Dialogue dialogue = new Dialogue(f,RdC.elementsTraduction[133], false);
		dialogue.setSize(120, 400);
		dialogue.pack();
		Toolkit k = Toolkit.getDefaultToolkit();
		Dimension tailleEcran = k.getScreenSize();
		int longeurEcran = tailleEcran.width;
		int hauteurEcran = tailleEcran.height;
		dialogue.setLocation(longeurEcran/2-dialogue.getSize().width/2, hauteurEcran/2-dialogue.getSize().height/2);
		dialogue.setAlwaysOnTop(true);
		dialogue.setResizable(false);
		
		dialogue.setVisible(true);
		
		while (!Stabiliser) {
			Stabiliser = true;
			for (int i = 0; i < this.ba.getBase().size(); i++) {
				es = this.ba.getBase().elementAt(i);
				this.pv.setNouvelElement(es.getContour());
				this.pv.setBase(STORAGE);
				this.pv.setK(1);
				Character lettre = this.pv.determine_lettre(this.pv
						.plusProchesVoisins2());
				if (lettre == es.getEtiquette()) {
					if (!appartient(i, poubelle)) {
						this.ba.getBase().removeElementAt(i);
						poubelle.add(i);
					}
				} else {
					Stabiliser = false;
					STORAGE.ajouter(es, true);
				}
			}
		}
		dialogue.setVisible(false);
		this.taille_fin = STORAGE.getBase().size();
		String masque = new String("#0");
		DecimalFormat form = new DecimalFormat(masque); 
		double fia=100-(double)((double)taille_fin/(double)taille_deb)*(double)100;
		String g=form.format(fia);
		ImageIcon reussite = new ImageIcon("./images/login.png");
		JOptionPane.showMessageDialog(null,
				RdC.elementsTraduction[134] +
				RdC.elementsTraduction[135]+taille_deb +
				RdC.elementsTraduction[136]+taille_fin +
				RdC.elementsTraduction[137]+g+"%",
				RdC.elementsTraduction[138], JOptionPane.INFORMATION_MESSAGE, reussite);
		
		return STORAGE;
	}
	
	
	public void suppression(int i,Vector<Vector<Integer>> distancesDejaCalculees) {
		//supprime la ieme distance dans distancesDejaCalculees
		for (int n  = 0;n < distancesDejaCalculees.size();n++){
			distancesDejaCalculees.elementAt(n).removeElementAt(i);
		}
		distancesDejaCalculees.removeElementAt(i);
	}
	
	
	public boolean appartient(int i, Vector<Integer> vi) {
		for (int j = 0; j < vi.size(); j++) {
			if (i == vi.elementAt(j)) {
				return true;
			}
		}
		return false;
	}

	public void setPV(ProchesVoisins pv) {
		this.pv = pv;
	}

	public int getTaille_deb() {
		return this.taille_deb;
	}

	public int getTaille_fin() {
		return this.taille_fin;
	}
}
