package baseApp;
import rdc.Panneau;
import rdc.RdC;

public class Etiquette extends javax.swing.JPanel {

	    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		/** Creates new form NewJPanel */
	    public Etiquette() {
	        initComponents();
	    }

	    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
	    private void initComponents() {

	        Panneau = new Panneau();
	        ajouterButton = new javax.swing.JButton();
	        charField = new javax.swing.JTextField();

	        Panneau.setPreferredSize(new java.awt.Dimension(123, 153));

	        javax.swing.GroupLayout PanneauLayout = new javax.swing.GroupLayout(Panneau);
	        Panneau.setLayout(PanneauLayout);
	        PanneauLayout.setHorizontalGroup(
	            PanneauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGap(0, 123, Short.MAX_VALUE)
	        );
	        PanneauLayout.setVerticalGroup(
	            PanneauLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGap(0, 153, Short.MAX_VALUE)
	        );

	        ajouterButton.setText(RdC.elementsTraduction[110]);
	        ajouterButton.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                ajouterButtonActionPerformed(evt);
	            }
	        });

	        charField.addActionListener(new java.awt.event.ActionListener() {
	            public void actionPerformed(java.awt.event.ActionEvent evt) {
	                charFieldActionPerformed(evt);
	            }
	        });

	        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
	        this.setLayout(layout);
	        layout.setHorizontalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap()
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
	                    .addGroup(layout.createSequentialGroup()
	                        .addComponent(charField, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
	                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                        .addComponent(ajouterButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	                    .addComponent(Panneau, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
	                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	        );
	        layout.setVerticalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(Panneau, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(charField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                    .addComponent(ajouterButton))
	                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	        );
	    }// </editor-fold>//GEN-END:initComponents

	    private void charFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_charFieldActionPerformed
	        // TODO add your handling code here:
	    }//GEN-LAST:event_charFieldActionPerformed

	    private void ajouterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ajouterButtonActionPerformed
	        // TODO add your handling code here:
	    }//GEN-LAST:event_ajouterButtonActionPerformed


	    // Variables declaration - do not modify//GEN-BEGIN:variables
	    private javax.swing.JButton ajouterButton;
	    private javax.swing.JTextField charField;
	    private javax.swing.JPanel Panneau;
	    // End of variables declaration//GEN-END:variables

}
