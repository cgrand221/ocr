/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package baseApp;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableCellRenderer;


/**
 *
 * @author gui
 */
public class ImageRenderer extends DefaultTableCellRenderer{
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void setValue(Object value) {
        if (value != null) {
            if (value instanceof Element) {
                Element em = (Element) value;
                if(em.etiquette!=' '){
                    //setText(em.chemin);
                    Image img;
					try {					
						img = ImageIO.read(new File(em.chemin));
						Image resizedImage = img.getScaledInstance(50, 50, 0);
						setIcon(new ImageIcon(resizedImage));
						} catch (IOException e) {
					
						}
                }
                else{
                    setText(value.toString());
                    setIcon(null);
                }
            } else {
                setText(value.toString());
            }
        } else {
            setText(" ");
        }
    }

}
