package baseApp;

import java.util.Comparator;




public class ComparateurElementsSurDistDajaCalc implements Comparator<Integer> {

	private BaseApprentissage ba;
	private Integer indiceComparaison;

	public ComparateurElementsSurDistDajaCalc(BaseApprentissage ba,
			Integer indiceComparaison) {
		this.ba = ba;
		this.indiceComparaison = indiceComparaison;
	}


	public int compare(Integer indiceElement1, Integer indiceElement2) {
		int tmp1 = ba.distancesDejaCalculees.elementAt(indiceComparaison).elementAt(indiceElement1);
		int tmp2 = ba.distancesDejaCalculees.elementAt(indiceComparaison).elementAt(indiceElement2);
		
		if (tmp1 < tmp2)
			return -1;
		if (tmp1 > tmp2)
			return 1;
		return 0;
	}

}
