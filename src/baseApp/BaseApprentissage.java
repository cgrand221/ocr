package baseApp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Vector;

import rdc.RdC;
import distanceEdition.*;



public class BaseApprentissage {
	public DistanceEdition de;
	public Vector<Element> base;
	public Vector<Vector<Integer>> distancesDejaCalculees;
	public String url = "base.ba";
	
	public BaseApprentissage(DistanceEdition de){
		this.base = new Vector<Element>();
		this.de = de;
		this.distancesDejaCalculees = new Vector<Vector<Integer>>();
	}

    
    public void ajouter(Element element, boolean calculDist){
        if(calculDist){
        Vector<Integer> tmp = new Vector<Integer>();
        if (this.base.size() == 0){
            tmp.add(new Integer(0));//pour le nouveau vecteur, j'ajoute les distances du nouveau element au elements deja enregistres
        }
        else {
            Vector<Short> motDejaEnregistre = null;
            for (int i = 0;i<this.base.size();i++){
                motDejaEnregistre = base.elementAt(i).getContour();
                this.distancesDejaCalculees.elementAt(i).add(new Integer(de.distance(motDejaEnregistre,element.getContour())));//pour les elements deja enregistres, j'ajoute la distance de l'element deja enregistre a l'element nouveau
                tmp.add(new Integer(de.distance(element.getContour(),motDejaEnregistre)));//pour le nouveau vecteur, j'ajoute les distances du nouveau element au elements deja enregistres
            }
            tmp.add(new Integer(0));
        }
        this.distancesDejaCalculees.add(tmp);//ajout des distances partant du nouveau element
        }
        this.base.add(element);
    }
	
	
	public void afficheDistancesDejaCalculees(){
		for (int i = 0;i<distancesDejaCalculees.size();i++){
			Vector<Integer> tmp = distancesDejaCalculees.elementAt(i);
			for (int j = 0;j < tmp.size();j++){
				System.out.print(tmp.elementAt(j)+" ");
			}
			System.out.println();
		}
	}
	
	
	public Vector<Element> getBase() {
		return base;
	}

	public void setBase(Vector<Element> base) {
		this.base = base;
	}
	
	
	
	
	
	public void lecture(String url) {

		if(url.contains(".")){
        String stmp[] = url.split("[/.]");
        url = stmp[stmp.length-2];
		}
		
        BufferedReader lecteur = null;
        String ligne;

        File base = new File(url+".ba");
        File distance = new File(url+".ddc");
        boolean test = true;
        

        this.distancesDejaCalculees = new Vector<Vector<Integer>>();
        
        if (distance.exists())
        {
        	test = false;
            try {
                lecteur = new BufferedReader(new FileReader(url+".ddc"));
                while ((ligne = lecteur.readLine()) != null) {
                    Vector<Integer> tmpv = new Vector<Integer>();
                    int entier = 0;
                    for (int i = 0; i < ligne.length(); i++) {
                         char tmp = ligne.charAt(i);
                         if (tmp != ' ')
                            entier = entier*10+Integer.parseInt((Character.toString(tmp)));
                         else {
                             tmpv.add(entier);
                             entier = 0;
                         }
                    }
                    this.distancesDejaCalculees.add(tmpv);

                }
                lecteur.close();
            } catch (FileNotFoundException exc) {
                System.out.println(RdC.elementsTraduction[63]);
            } catch (IOException e) {
                System.out.println(RdC.elementsTraduction[64]);
            }
            System.out.println(url+RdC.elementsTraduction[98]);
        }

       
       
        if (base.exists()) {
            try {
                lecteur = new BufferedReader(new FileReader(url+".ba"));
                while ((ligne = lecteur.readLine()) != null)
                {   	
                	String tab[] = ligne.split(" ");        	
                	Element element = new Element();   	
                	element.setEtiquette(tab[0].charAt(0));         	
                	element.ajout(tab[1]); 
                	element.setChemin(tab[2]);
                	ajouter(element, test);
                }

                lecteur.close();
            } catch (FileNotFoundException exc) {
                System.out.println(RdC.elementsTraduction[63]);
            } catch (IOException ioe) {
                System.out.println(RdC.elementsTraduction[64]);
            } catch (NumberFormatException e) {
                System.out.println(RdC.elementsTraduction[99]);   
            }
        }


    }

	    
	  public void ecriture(String url){

		  if(url.contains(".")){
			  String stmp[] = url.split("[/.]");
			  url = stmp[stmp.length-2];
		  }
          
		  PrintWriter ecrivainBase, ecrivainDdc;
		  try {
			ecrivainBase =  new PrintWriter(new BufferedWriter(new FileWriter(url+".ba")));
            ecrivainDdc = new PrintWriter(new BufferedWriter(new FileWriter(url+".ddc")));
            Enumeration<Element> elements = this.base.elements();
            while(elements.hasMoreElements()){
			  Element element = elements.nextElement();
			  ecrivainBase.print(element.etiquette+" ");		  
			  Enumeration<Short> contour = element.getContour().elements();
			  while(contour.hasMoreElements())
				  ecrivainBase.print(contour.nextElement());
              ecrivainBase.print(" "+element.getChemin());
			  ecrivainBase.println();
		  }
		  ecrivainBase.close();

          for(int i=0;i<distancesDejaCalculees.size();i++){
              for(int j=0;j<distancesDejaCalculees.elementAt(i).size();j++){
                   ecrivainDdc.print(distancesDejaCalculees.elementAt(i).elementAt(j)+" ");
              }
              ecrivainDdc.println();
          }
          ecrivainDdc.close();

		  } catch (IOException e) {
			  System.out.println(RdC.elementsTraduction[64]);
		  }
	  }
	 	
}
	

