/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package baseApp;

import java.io.File;

import rdc.RdC;

/**
 *
 * @author gui
 */
public class FiltreFichier extends javax.swing.filechooser.FileFilter{

    public static final String extension = ".ba";
    @Override
    public boolean accept(File f) {
        String s = (f.toString());
		return (s.endsWith(extension));
    }

    @Override
    public String getDescription() {
        return RdC.elementsTraduction[3]+extension;
    }



}
