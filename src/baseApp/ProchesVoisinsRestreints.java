package baseApp;


import java.util.Hashtable;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;


public class ProchesVoisinsRestreints {
	int k;
	BaseApprentissage base;
	private static final long serialVersionUID = 1L;
	public ProchesVoisinsRestreints(Vector<Short> nouvelElement,BaseApprentissage base, distanceEdition.DistanceEdition distance_ed,int k){
		this.k = k;
		this.base = base;
	}

	public boolean classify(Vector<Element> s1, Vector<Element> s2,Vector<Integer> ordre1,Vector<Integer> ordre2,TreeSet<Integer> elementsSupprimes,Hashtable<Character,Integer> frequenceEtiquette) {
		//Classify s1 with s2
		//Remove from S1 the misclassified instances
		boolean stabilization;

		//this.base.setBase(s2);
		
		stabilization = true;
		int i;
		i = 0;
		while (i<s1.size()){
			Element e = s1.elementAt(i);
			
			Vector<Character> etiquettes = plusProchesVoisins2a(ordre1,ordre2,i);
			Character etiquette = etiquettes.elementAt(0);
			//System.err.println(frequenceEtiquette.get(e.getEtiquette()));
			if ((!etiquette.equals(e.getEtiquette())) && (((Integer) frequenceEtiquette.get(e.getEtiquette())) >= 1)){
				frequenceEtiquette.put(e.getEtiquette(), ((Integer)frequenceEtiquette.get(e.getEtiquette()))-1);
				stabilization = false;			
				elementsSupprimes.add(ordre1.elementAt(i));
				s1.removeElementAt(i);
				ordre1.removeElementAt(i);
			}
			else i++;
		}

		return stabilization;
	}
	public Vector<Character> plusProchesVoisins2a(Vector<Integer> ordre1,Vector<Integer> ordre2,int indiceContourAEtiquetter){
		//meme chose que plusProchesVoisins2 mais utilise les distances deja calculees => ne calcule aucune distance
		//a utiliser seulement lorsque le mot a etiqueter est deja dans la base
		Vector<Character> tab_caractere = new Vector<Character>();
		TreeSet<Integer> tab = new TreeSet<Integer>(new ComparateurElementsSurDistDajaCalc(base, ordre1.elementAt(indiceContourAEtiquetter)));
		int i;
		i = 0;
		while ((i < ordre2.size()) && (i < this.k)){//ajout de k elements : pas necessairement les plus petits
			tab.add(ordre2.elementAt(i));
			i++;
		}


		boolean[] tags = new boolean[ordre2.size()-this.k];//si tags[i] est faux, il n'est pas necessaire de calculer la distance du nouvelElement à l'élément i+k de la base
		for (i = 0;i < tags.length;i++){
			tags[i] = true;
		}
		int j,d1,d2;
		int distanceAMax,distanceAI;
		int max;
		i = this.k;
		j = 0;
		while (i < ordre2.size()){//ajout du reste que si necessaire
			if (tags[j]){
				max = tab.last();
				distanceAI = base.distancesDejaCalculees.elementAt(ordre1.elementAt(indiceContourAEtiquetter)).elementAt(ordre2.elementAt(i));
				distanceAMax = base.distancesDejaCalculees.elementAt(ordre1.elementAt(indiceContourAEtiquetter)).elementAt(max);
				if (distanceAI>distanceAMax){//si ca ne fonctionne pas on peut accelerer le calcul pour les cas suivants en supprimant les cas impossibles
					//System.out.println("acceleration");
					//comme l'element a l'indice i ne convient pas :
					//on peut dire que tous les points suffisements proches de l'element a l'indice i
					//n'appartiendront pas aux k plus proches voisins de l'element nouveau
					//de meme tous les elements suffisement eloignes de de l'element a l'indice i
					//n'appartiendront pas aux k plus proches voisins de l'element nouveau

					//suffisament proche de i c'est en faite : distance de i aux points < distance(nouveau,i) - distance(nouveau,max)
					//suffisament eloigne de i c'est en fait : distance de i aux points > distance(nouveau,i) + distance(nouveau,max)
					d1 = distanceAI - distanceAMax;
					d2 = distanceAI + distanceAMax;
					//System.err.println("ttaille2"+base.distancesDejaCalculees.size);
					
					for(int l = i;l<ordre2.size();l++){
						//System.err.println("ttaille"+this.base.distancesDejaCalculees.size());
						//System.err.println("ttaille2"+this.base.distancesDejaCalculees.elementAt(i).size());
						
						if ((base.distancesDejaCalculees.elementAt(ordre2.elementAt(i)).elementAt(ordre2.elementAt(l)) < d1) || (base.distancesDejaCalculees.elementAt(ordre2.elementAt(i)).elementAt(ordre2.elementAt(l)) > d2)){
							tags[l-this.k] = false;
						}
					}	

				}
				else {
					tab.add(ordre2.elementAt(i));
				}
			}
			i++;
			j++;
		}
		Iterator<Integer> it = tab.iterator();
		j = 0;
		while ((it.hasNext()) && (j < this.k)) {
			tab_caractere.add(base.getBase().elementAt(it.next()).getEtiquette());
			//System.err.println("j="+j);
			j++;
		}
		return tab_caractere;
	}

	
}
