package distanceEdition;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;

import javax.swing.*;

import rdc.RdC;

import baseApp.BaseApprentissage;
import baseApp.BaseInterface;
import baseApp.Element;




public class DistanceEdition extends JPanel implements ActionListener{
	/**
	 * 
	 */

	public void setBaseApprentissage(BaseInterface bi){
		this.ba = bi.ba;
	}


	private static final long serialVersionUID = 1L;
	int[][] poidsEntier;
	public static final String epsilon = ((new String("\u0395")).toLowerCase());
	public static final Dimension dimentionPoids = new Dimension(300,300);
	JTextField[][] poids;
	JButton poidsNormal,poidsDeBase,ouvrir,enregistrer,appliquer;
	public BaseApprentissage ba;
	public DistanceEdition() {
		super();
		JPanel panneau = new JPanel();
		panneau.setLayout(new GridLayout(5,1));
		poidsNormal = new JButton(RdC.elementsTraduction[84]);
		poidsDeBase = new JButton(RdC.elementsTraduction[85]);
		ouvrir = new JButton(RdC.elementsTraduction[86]);
		enregistrer = new JButton(RdC.elementsTraduction[87]);
		appliquer = new JButton(RdC.elementsTraduction[88]);
		poidsNormal.addActionListener(this);
		poidsDeBase.addActionListener(this);
		ouvrir.addActionListener(this);
		enregistrer.addActionListener(this);
		appliquer.addActionListener(this);
		panneau.add(poidsNormal);
		panneau.add(poidsDeBase);
		panneau.add(ouvrir);
		panneau.add(enregistrer);
		panneau.add(appliquer);
		this.add(panneau);

		JPanel panneau2 = new JPanel();

		panneau2.setLayout(new GridLayout(11,10));

		int i,j;

		this.poids = new JTextField[9][];
		this.poidsEntier = new int[9][];
		for (i = 0;i < 9;i++){
			this.poids[i] = new JTextField[9];
			this.poidsEntier[i] = new int[9];
			for(j=0;j<9;j++){
				this.poids[i][j] = new JTextField(poidsNormal(i,j)+"");
				this.poidsEntier[i][j] = poidsNormal(i,j);				
			}
		}

		panneau2.add(new JLabel());
		panneau2.add(new JLabel(epsilon));

		for (i = 0;i < 8;i++){
			panneau2.add(new JLabel(""+i));
		}
		panneau2.add(new JLabel(epsilon));

		for (j = 0;j < 9;j++){
			panneau2.add(this.poids[0][j]);
		}
		for (i = 1;i < 9;i++){
			panneau2.add(new JLabel(""+(i-1)));
			for (j = 0;j < 9;j++){
				panneau2.add(this.poids[i][j]);
			}
		}

		panneau2.setPreferredSize(dimentionPoids);
		this.add(panneau);
		this.add(panneau2);		
		this.setVisible(true);
	}

	public void recalculeDistanceDejaCalculees(){
		ba.distancesDejaCalculees = new Vector<Vector<Integer>>();
		for (int i = 0; i < ba.base.size();i++){
			ajoutDistanceDejaCalculees(ba.base.elementAt(i));
		}
	}

	public void ajoutDistanceDejaCalculees(Element element){
		Vector<Short> motDejaEnregistre = null;
		Vector<Integer> tmp = new Vector<Integer>();
		for (int i = 0;i<this.ba.distancesDejaCalculees.size();i++){
			motDejaEnregistre = ba.base.elementAt(i).getContour();
			ba.distancesDejaCalculees.elementAt(i).add(new Integer(this.distance(motDejaEnregistre,element.getContour())));//pour les elements deja enregistres, j'ajoute la distance de l'element deja enregistre a l'element nouveau
			tmp.add(new Integer(this.distance(element.getContour(),motDejaEnregistre)));//pour le nouveau vecteur, j'ajoute les distances du nouveau element au elements deja enregistres
		}
		tmp.add(new Integer(0));
		ba.distancesDejaCalculees.add(tmp);
	}



	private int poidsNormal(int i,int j){
		//i et j sont les indices dans la matrice de poids
		//si i=0 ou j=0 on renvoie 1 ce qui est le poids d'une insertion ou d'une suppression
		//sinon on renvoie le poids de subtitution du nombre (i-1) en (j-1) a l'aide la rosace
		if ((i == 0) && (j == 0))
			return 0;
		if ((i == 0) || (j == 0)){
			return 1;
		}
		i = Math.abs(i - j);//angle par rapport a 0 qui va de 0 a 7
		//7 c'est 1
		//6 c'est 2
		//5 c'est 3
		//4 ne change pas

		if (i > 4)
			i = 8 - i;

		if (i == 0)
			return 0;
		return i+1;
		//+1 car une substition doit etre plus lourde 
		//qu'une insertion ou une suppression
	}

	protected void paintComponent(Graphics g) {

		super.paintComponent(g);

	}
	public int distance(Vector<Short> u,Vector<Short> v){
		int[][] d;
		int i,j,d1,d2,d3;
		d = new int[u.size()+1][];

		d[0] = new int[v.size()+1];
		d[0][0] = 0;
		for(j=1;j<=v.size();j++){
			d[0][j] = d[0][j-1]+poidsEntier[0][v.elementAt(j-1)+1];
		}
		for (i = 1;i <= u.size();i++){
			d[i] = new int[v.size()+1];	
			d[i][0] = d[i-1][0];
			d[i][0] += poidsEntier[u.elementAt(i-1)+1][0];
			for(j = 1;j <= v.size();j++){
				d1 = d[i-1][j-1]+poidsEntier[u.elementAt(i-1)+1][v.elementAt(j-1)+1];//substitution
				d2 = d[i-1][j]+poidsEntier[0][v.elementAt(j-1)+1];//insertion
				d3 = d[i][j-1]+poidsEntier[u.elementAt(i-1)+1][0];//suppression
				d[i][j] = min(d1,d2,d3);
			}
		}
		/*
		for (i = 0;i<=u.size();i++){
			for(j=0;j<=v.size();j++){
				System.out.print(d[i][j]);
			}
			System.out.println();
		}

		System.out.println();
		System.out.println();
		System.out.println();
		 */
		return d[u.size()][v.size()];
	}
	public static int min(int a,int b,int c){
		if ((a <= b) && (a <= c)){
			return a;
		}
		if ((b <= a) && (b <= c)){
			return b;
		}
		return c;
	}



	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() == poidsNormal){
			int i,j;
			for (i = 0;i < 9;i++){
				for(j=0;j<9;j++){
					this.poids[i][j].setText(poidsNormal(i,j)+"");
				}
			}
		}
		else if (ae.getSource() == poidsDeBase){
			int i,j;
			for (i = 0;i < 9;i++){
				for(j=0;j<9;j++){
					this.poids[i][j].setText(poidsDeBase(i,j)+"");
				}
			}
		}
		else if (ae.getSource() == ouvrir){
			int i,j;
			JFileChooser jfc = new JFileChooser();
			jfc.setFileFilter(new FiltreFichier());
			jfc.setAcceptAllFileFilterUsed(false);
			if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
				File fichier = new File(jfc.getSelectedFile().toString());
				FileInputStream fis=null;

				try {
					fis = new FileInputStream(fichier);
					BufferedInputStream bis= new BufferedInputStream(fis);
					for (i=0;i<9;i++){
						for(j=0;j<9;j++){							
							this.poids[i][j].setText(bis.read()-'0'+"");							
						}
						bis.read();
					}					
					bis.close();					
				} catch (FileNotFoundException e) {
					JOptionPane.showMessageDialog(this, RdC.elementsTraduction[89],RdC.elementsTraduction[90],JOptionPane.ERROR_MESSAGE);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(this, RdC.elementsTraduction[91],RdC.elementsTraduction[92],JOptionPane.ERROR_MESSAGE);
				}

			}
		}
		else if (ae.getSource() == enregistrer){
			int i,j;
			JFileChooser jfc = new JFileChooser();
			jfc.setFileFilter(new FiltreFichier());
			jfc.setAcceptAllFileFilterUsed(false);
			if (jfc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION){
				String s = jfc.getSelectedFile().toString();
				if (!s.endsWith(FiltreFichier.extension))
					s = s+FiltreFichier.extension;
				File fichier = new File(s);				
				FileOutputStream fos=null;

				try {
					fos = new FileOutputStream(fichier);
					BufferedOutputStream bos= new BufferedOutputStream(fos);
					for (i=0;i<9;i++){
						for(j=0;j<9;j++){							
							bos.write(Integer.parseInt(this.poids[i][j].getText())+'0');							
						}
						bos.write('\n');
					}					
					bos.close();					
				} catch (FileNotFoundException e) {
					JOptionPane.showMessageDialog(this, RdC.elementsTraduction[89],RdC.elementsTraduction[90],JOptionPane.ERROR_MESSAGE);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(this, RdC.elementsTraduction[91],RdC.elementsTraduction[92],JOptionPane.ERROR_MESSAGE);
				}

			}
		}
		else if (ae.getSource() == appliquer){
			this.faireAppliquerLesPoids();			
			recalculeDistanceDejaCalculees();
			//this.ba.afficheDistancesDejaCalculees();
		}


	}

	public void faireAppliquerLesPoids(){
		int i,j;
		for (i = 0;i<9;i++){
			for(j=0;j<9;j++){
				this.poidsEntier[i][j] = Integer.parseInt(this.poids[i][j].getText());
			}
		}
	}
	private int poidsDeBase(int i, int j) {
		//i et j sont les indices dans la matrice de poids
		//si i=0 ou j=0 on renvoie 1 ce qui est le poids d'une insertion ou d'une suppression
		//sinon on renvoie le poids de subtitution du nombre (i-1) en (j-1) : 2
		if ((i == 0) && (j == 0))
			return 0;
		if ((i == 0) || (j == 0))
			return 1;
		if (i == j)
			return 0;
		return 2;
	}
}
