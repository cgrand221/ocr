compiler:
	javac -classpath "lib/*:src" src/rdc/RdC.java


lancer:
	java -classpath "lib/*:src" rdc/RdC


propre:
	rm src/baseApp/*.class
	rm src/dico/*.class
	rm src/distanceEdition/*.class
	rm src/gestionLangage/*.class
	rm src/motus/*.class
	rm src/rdc/*.class
	rm src/voisins/*.class


